//Header-Def von valderN

/*Johannes Bogon 2835480
Michael Lies 2669805*/


#ifndef VALDERN_HPP
#define VALDERN_HPP

#include <iostream>
#include <cmath>

class valderN {
public:
//hier kommen die Methoden rein

//Konstruktoren
  valderN(const double _val, const unsigned int i);
  valderN(const double _val);
  valderN(const valderN& other);

//get und set
  double getVal(void) const;
  double* getDer(void) const;
  void setVal(double _val);
  void setDer(double* _der);

//Operatorüberladungen
  valderN& operator=(const valderN& rhs);  // assignment operator
  valderN  operator-(void) const;          // unary (!) minus
  valderN& operator-=(const valderN& rhs); // compound operator -=
  valderN& operator+=(const valderN& rhs); // compound operator +=
  valderN& operator*=(const valderN& rhs); // compound operator *=
  // friend functions
  friend valderN operator-(valderN lhs, const valderN& rhs);
  friend valderN operator+(valderN lhs, const valderN& rhs);
  friend valderN operator*(valderN lhs, const valderN& rhs);
  friend std::ostream& operator<<(std::ostream& os, const valderN& rhs);

private:
//hier kommen die Attribute rein
  double val; //Funktionswert
  unsigned int N; //Anzahl Variablen
  double* der; //Gradient

};

#endif