//cpp-File für Klasse valderN

/*Johannes Bogon 2835480
Michael Lies 2669805*/


#include "valderN.hpp"

//Konstruktur zu x_i
valderN::valderN(double _val, const unsigned int i) {
    val = _val;
    //da wir nur ein i haben, bekommt das objekt die Länge i
    N = 2; //nicht die elgeganteste Art, N zu fixieren, aber sollte passen
    der = new double[N];
    //da wir bei x_1 anfangen, aber die vektoren bei null starten, anpassen:
    for (unsigned int k = 0; k<N;k++) {
        if (k == i-1) {
            der[k] = 1;
        }
        else {
            der[k] = 0;
        }
    }
}

//Konstruktor zu Konstante
valderN::valderN(const double _val) {
    val = _val;
    N = 2; //again, kann man bestimmt schöner machen
    der = new double[N];
    for (unsigned int k = 0;k<N;k++) {
        der[k] = 0;
    }

}

//Copy-Konstruktor
/*keine Gröeßenprüfung, da ich ja immer mit Objekten der Klasse N=2 arbeite, also
beide zwangsläufig gleich große Vektoren in der liegen haben*/
valderN::valderN(const valderN& other) {
    N = 2; //oder was auch immer
    val = other.val;
    der = new double[N];
    for (unsigned int k = 0;k<N;k++) {
        der[k] = other.der[k];
    }
}

//Get und Set:
double valderN::getVal(void) const {
  return val;
}
double* valderN::getDer(void) const {
  return der;
}
void valderN::setVal(double _val) {
  val = _val;
}
void valderN::setDer(double* _der) {
  for (unsigned int k=0;k<N;k++) {
    der[k] = _der[k];
  }
}

//Operatoren:
valderN& valderN::operator=(const valderN& rhs) {
    //kann genau wie in valder gemacht werden
  if (this != &rhs) { // do something only if no self-assignment
    val = rhs.val;
    der = rhs.der;
    N = rhs.N;
  }
  return *this;
}
valderN valderN::operator-(void) const {
    //anpassen aus valder durch iteration der datenpunkte
    valderN newvalderN = valderN((*this));
    for (unsigned int k=0;k<N;k++) {
        der[k] = -der[k];
    }
  return newvalderN;
}
valderN& valderN::operator-=(const valderN& rhs) {
  //ebenso: müssen durch der iterieren
  val -= rhs.val;
  for (unsigned int k = 0;k<N;k++) {
    der[k] -= rhs.der[k];
  }
  return *this;
}
valderN& valderN::operator+=(const valderN& rhs) {
  val += rhs.val;
  for (unsigned int k=0;k<N;k++) {
    der[k] += rhs.der[k];
  }
  return *this;
}
valderN& valderN::operator*=(const valderN& rhs) {
  //grad(f*g) = f*grad(g) + g*grad(f) - nachbauen
  /*Note: über das Speichern von einer einzigen value wirds Mist, wenn eine von beiden
  in x1 und x2 definiert ist, aber da komm ich nicht drum rum
  */
  for (unsigned int k=0;k<N;k++) {
    double der_res = (this->val)*rhs.der[k] + (this->der[k]*rhs.val);
    der[k] = der_res;
  }  
  val *= rhs.val;
  return *this;
}

valderN operator-(valderN lhs, const valderN& rhs) {
  lhs -= rhs;
  return lhs;
}
valderN operator+(valderN lhs, const valderN& rhs) {
  lhs += rhs;
  return lhs;
}
valderN operator*(valderN lhs, const valderN& rhs) {
  lhs *= rhs;
  return lhs;
}

std::ostream& operator<<(std::ostream& os, const valderN& x) {
  os << '(' << x.val << ", [";
  for (unsigned int k=0;k<2;k++) {
    os << " "<< x.der[k] <<" ";
  }
  os << "])" << std::endl;
  return os;
}