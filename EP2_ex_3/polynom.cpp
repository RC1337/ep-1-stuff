/*Kommentarblock:
- im vorgegebenen test, wo ein Polynom mit einem complex multipliziert wird, funktioniert es in
  meiner Implementierung nicht, weil der Multiplikator * explizit polynome als input erwartet
  und complex-variablen nicht als polynome erkannt werden
  -> kann man tendenziell irgendwie in der funktionsdef fixen, aber die saubere variante ist:
   Multiplikation mit * nur zwischen Funktionen, für Multiplikation mit komplexer Zahl eigene
   Funktion erstellen (in der aus complex ein polynom vom grad null wird) - ich mach es im test
   händisch
- selber Ansatz bei der set-coeff function
- In der Setdegreeatleast-function: An sich hätte ich gedacht, dass man beim überschreiben free[] auf
  data schicken muss, wenn ich das allerdings tue, bekomme ich Fehlermeldungen, lasse ich es weg,
  passiert was ich erwartet hätte - muss das so sein oder habe ich irgendwas beim Löschen falsch?
  */

#include "polynom.hpp"
#include <iostream>
#include <iomanip>

polynom::polynom(void) {
  size = 0;
  data = nullptr;
}

polynom::polynom(const unsigned int deg) {
  size = deg;
  data = new complex[deg+1];
  for (unsigned int i=0;i<=deg;i++) {
    data[i] = complex(0.0); //egal was, ich akzeptiere nur eingaben als complex
  }
}

polynom::polynom(const unsigned int deg, const complex* coeff) {
  size = deg;
  data = new complex[deg+1];
  for (unsigned int i=0;i<=deg;i++) {
    data[i] = coeff[i];
  }
}

polynom::polynom(const complex coeff) {
  size = 0;
  data = new complex;
  *data = coeff;
}

polynom::polynom(const polynom& other) {
  size = other.size;
  data = other.data;
}

polynom::~polynom(void) {
  if (data != nullptr) {
    delete[] data;
    data = nullptr;
    size = 0;
  }
}

void polynom::setDegreeAtLeast(const unsigned int deg) {
  //falls leeres Polynom: einfach Nullpolynom von grad deg initieren
  if (data == nullptr) {
    (*this) = polynom(deg);
  }
  else { 
    if (size < deg) { 
      //neue daten klarmachen
      complex* new_data = new complex[deg+1];
      for (unsigned int i=0;i<=deg;i++) {
        if (i<=size) {
          new_data[i] = data[i];
        }
        else {
          new_data[i] = complex(0);
        }
      }
      //delete[] data;
      size = deg;
      data = new_data;
    }
  }
}

void polynom::setCoeff(const unsigned int i, const complex coeff) {
  if (size < i) {
    this->setDegreeAtLeast(i); //ggf auf richtigen grad bringen
  }
  data[i] = coeff;
}


polynom& polynom::operator=(const polynom& rhs) {
  if (size == rhs.size) {
    for (unsigned int i = 0;i<= size;i++) {
      data[i] = rhs.data[i];
    }
  }
  else {
    delete[] data;
    size = rhs.size;
    data = new complex[rhs.size+1];
    for (unsigned int i = 0;i<= size;i++) {
      data[i] = rhs.data[i];
    }
  }
  return (*this);
}

polynom operator+(polynom lhs, const polynom& rhs) {
  //ggf die degrees erhöhen
  if (rhs.size > lhs.size) {
    lhs.setDegreeAtLeast(rhs.size);
  }
  for (unsigned int i=0;i<=rhs.size;i++) {
    lhs.data[i] = lhs.data[i] + rhs.data[i];
  }
  return lhs;
}

polynom operator*(const polynom& lhs, const polynom& rhs) {
  //hilfspolynom erstellen
  unsigned int grad = lhs.size + rhs.size; //bei multiplik addieren sich expos
  polynom res = polynom(grad);
  //Multiplikation - über zwei Schleifen
  for (unsigned int i=0;i<=lhs.size;i++) {
    for (unsigned int j=0;j<=rhs.size;j++) {
      //bestimmen, wo es reinsoll
      unsigned int index = i+j; //Grade addieren sich jeweils
      res.setCoeff(index,(res.data[index] + lhs.data[i]*rhs.data[j]));
    }
  }
  return res;
}

complex polynom::operator()(const complex z) const {
  //auswerten an Stelle z
  complex val;
  unsigned int grad = (*this).size; //um oben zu starten
  //erster Step: val = p_n
  val = (*this).data[grad];
  for (int i = grad - 1;i>=0;i--) {
    val = val*z + (*this).data[i];
  }



  unsigned int max = (*this).size;
  val = (*this).data[max];
  for (int i = max-1;i>=0;i--) {
    val = val*z + (*this).data[i];
  }
  return val;
}

std::ostream& operator<< (std::ostream& os, const polynom& rhs) {
  unsigned int first = 0;
  for (unsigned int i =0;i<=rhs.size;i++) {
    //schau ob der Wert null ist
    double reval = rhs.data[i].getRe();
    double imval = rhs.data[i].getIm();
    if (!(reval == 0 && imval == 0)) {
      if (first == 0) {
        //damit beim ersten term kein pluszeichen kommt
        os << "[ " << rhs.data[i] << " ]*z^" << i;
      }
      else {
        os << " + [ " << rhs.data[i] << " ]*z^" << i;
      }
      first = 1; //technically würds reichen den einmal zu setzen
    }
  }
  if (first == 0) {
    //polynom war leer
    os << "0";
  }
  os << std::endl;
  return os;
}


int main(void) {
  //TODO: Implement your tests as desired.
  std::cout << "Implement your own tests in main() as you like it." << std::endl;
  
  // some test suggestions
  
  complex data[4] = {2., complex(0.,1.), 0., complex(1.,1.) };
  polynom P1(3, data);
  std::cout << std::fixed << "P1(X)=" << P1 << std::endl;
  polynom P2 = P1*P1;
  polynom P4 = polynom(2);
  P4.setCoeff(1,complex(1.0,1.0));
  P4.setCoeff(2,complex(0.0,1.0));
  P4.setCoeff(0,complex(1.0,0.0));
  std::cout << "P2(X)=" << P2 << std::endl;
  complex multi[1] = {complex(0.,4.)};
  polynom compol = polynom(0,multi);
  polynom P3 = P1 * compol + P2;
  std::cout << "P3(X)=" << P3 << std::endl; 
  P3.setCoeff(15, complex(1.,0.0));
  std::cout << "P3(X)=" << P3 << std::endl;
  std::cout << "P1(17+2i)^2=" << P1(complex(17,2))*P1(complex(17,2)) << std::endl;
  std::cout << "P2(17+2i)  =" << P2(complex(17,2)) << std::endl;
  
  // should give output like
  /*
P1(X)=(1.000000 + i*(1.000000))*X^3+(0.000000 + i*(1.000000))*X+(2.000000 + i*(0.000000))
P2(X)=(0.000000 + i*(2.000000))*X^6+(-2.000000 + i*(2.000000))*X^4+(4.000000 + i*(4.000000))*X^3+(-1.000000 + i*(0.000000))*X^2+(0.000000 + i*(4.000000))*X+(4.000000 + i*(0.000000))
P3(X)=(1.000000 + i*(0.000000))*X^15+(0.000000 + i*(2.000000))*X^6+(-2.000000 + i*(2.000000))*X^4+(0.000000 + i*(8.000000))*X^3+(-1.000000 + i*(0.000000))*X^2+(-4.000000 + i*(4.000000))*X+(4.000000 + i*(8.000000))
P1(17+2i)^2=-32730015.000000 + i*(38492632.000000)
P2(17+2i)  =-32730015.000000 + i*(38492632.000000)
  */
  return 0;
}


