//File zum testen von valder

/*Johannes Bogon 2835480
Michael Lies 2669805
*/

#include <iostream>
#include <iomanip>
#include <limits>
#include <cmath>
#include "valderN.hpp"

int main(void) {
    valderN x1 (1.0 , 1);
    valderN x2 (3.5 , 2);
    valderN sinx2 (std::sin(3.5),2);
    double* dero = new double[2];
    sinx2.setDer(dero);


    //f = 6.0*x1*std::sin(x2)+std::pow(x1*x2,3);
    //das oben das gewünschte f
    //jetzt: das im Rahmen der valderN-Notation auch so eingeben
    valderN f = x1*x1*x1*x2*x2*x2+6*x1*sinx2;
    //Ergebnis von val passt
    //Gradient: passt ebenfalls
    

    std::cout << "(f(1, 3.5) ; grad f(1, 3.5) ) = " << f << std::endl;
    return 0;
}