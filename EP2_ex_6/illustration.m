%Johannes Bogon 2835480, Michael Lies 2669805

function [] = illustration
%Zur Illustration des Differenzenquotienten

%Strukt mit h-Werten, Diffquot-Werten und Fehler erstllen
h = logspace(-12,-2,100);
structDQ = struct('h',h);
dq = h; %alternativ hätte ich auch hunderter-vektor machen können, aber copy tuts auch
f = @(x) exp(x);
for i=1:100
    dq(i) = differenzenquotient(f,0,h(i));
end
structDQ.dq = dq;
corr_val = ones(1,100); %Ableitung von e^x and Stelle 0 ist 1
diff = abs(corr_val-dq);
structDQ.fehler = diff;

%Tabelle
format shortE;
Schrittweite = structDQ.h(1:10:end)';
Differenzenquotient = structDQ.dq(1:10:end)';
Fehler = structDQ.fehler(1:10:end)';
T = table(Schrittweite,Differenzenquotient,Fehler);
titel = 'Verhalten von Differenzenquotient und Fehler bei versch. Schrittweite';
disp(sprintf('%60s',titel));
disp(T);

%Plot
figure
loglog(structDQ.h,structDQ.fehler);
title('Fehlerverhalten abhängig von Schrittweite')
xlabel('Schrittweite h')
ylabel('Fehler im Differenzenquotient')
grid on
end