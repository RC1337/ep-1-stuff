%Johannes Bogon 2835480, Michael Lies 2669805

function fquot = differenzenquotient(f,x,h)
%Berechnet Differenzenquotient von f an Stelle x mit Schrittweite h
%   Detailed explanation goes here
if h<=0
    error("ungültige Schrittweite")
else
    fquot = (f(x+h)-f(x))/h;
end
end
