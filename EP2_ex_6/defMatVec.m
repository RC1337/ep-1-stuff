%Johannes Bogon 2835480, Michael Lies 2669805

%Zeilenvektor mit ersten fünf geraden Zahlen
g = [0:2:8];
%Spaltenvektor mit ersten fünf ungeraden Zahlen
u = [1:2:9]';

%2x5 Matrix, g in erste, u in zweite, dimension und anzahl einträge a
A = [g; u'];
[n,m] = size(A) %als return Ausgabe
num_of_elts = numel(A) %Elemente return als Ausgabe

%Matrix mit Einsern und Primzahlen
%100 - 500 ste, also dim = 400
B = ones(400,400);
p = primes(4567);
p = p(end-399:end);
p = p';
for i=1:400
    B(i,1) = p(i);
end

%diag matrix
a = -2.01;
a_vec = a*ones(100,1);
sub_diag = ones(99,1);
top_diag = ones(99,1);
top_diag(1) = 2;
C = diag(a_vec);
Sub = diag(sub_diag,-1);
Top = diag(top_diag,1);
C = C + Sub + Top;