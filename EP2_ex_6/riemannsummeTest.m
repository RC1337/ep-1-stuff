%Johannes Bogon 2835480, Michael Lies 2669805

%Test für riemannsumme
a = 0;
b = pi/2;
epsilon = 1*10^(-6);
k = 5;
m_max = 50;
f = @(x) 5*(exp(pi)-2)^(-1)*exp(2*x)*cos(x);

val = riemannsumme(f,a,b,epsilon,k,m_max)