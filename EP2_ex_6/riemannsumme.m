%Johannes Bogon 2835480, Michael Lies 2669805

function out = riemannsumme(f,a,b,epsilon,k,m_max)
%berechnet integral f von a bis b über riemannsumme
%  gewohnte Bildung von Riemannintegral über Summengrenzwert
% f - zu integrierende Funktion
% a- untere Grenze
%b - obere Grenze
%epsilon - Genauigkeitsofrderung
%k - Basisschrittweite
%m_max - obere Schranke f Summenbild

%Definition von Hilfsvariablen
S_m1k = 0; %neue Summe

for i = 1:m_max %maximal m_max mal die ganze Sache machen
    S_mk = S_m1k; %shift: neue SUmme aus letztem Schritt is jetzt alte Summe
    %Wert der neuen Summe berechnen
    S_m1k = 0;
    n = i*k;
    for j=0:(n-1)
        S_m1k = S_m1k + f(a+(j+0.5)*(b-a)/n);
    end
    S_m1k = (b-a)/n*S_m1k;
    diff = abs(S_mk-S_m1k);
    if (diff <= epsilon)
        break;
    end
end
out = S_m1k;
if diff > epsilon
    warning("Gewünschte Genauigkeit für Ergebnis nicht gegegeben")
end
end