/*Input: datei source, ein pos integer je zeile
Output: datei target, worin zeilen drin sind, wo sich durch erste von source teilen lassen*/

#include <stdio.h>
#include <stdlib.h>

int main (void) {
    //Pointer zu den dateien
    FILE *ifptr, *ofptr;

    //input source.txt aufmachen und lesen, relativpfade kein problem da selber ordner
    ifptr = fopen("source.txt", "r");
    //info fals gibt nicht
    if (ifptr == NULL) {
        fprintf(stderr, "Kann input-file >source.txt< nicht aufmachen");
    }
    //output target.txt aufmachen, mit schreibzugriff
    ofptr = fopen("target.txt", "w");
    //info falls fehler dabei
    if (ofptr == NULL){
        fprintf(stderr, "Kann output-file >target.txt> nicht aufmachen");
    }

    //durch die zeilen von source iterieren
    int num;
    int teiler;
    int init = 1;
    while (fscanf(ifptr,"%i",&num)>0) {
        if (init == 1) {
            //init, such die zahl zum durch teilen
            teiler = num;
            init = 0;
        }
        else {
            //check teilbarkeit, falls teilbar schreib in target
            if (num % teiler == 0) {
                fprintf(ofptr,"%i \n",num);
            }

        }
    }

    //dateien schließen
    fclose(ifptr);
    fclose(ofptr);


    return 0;
}