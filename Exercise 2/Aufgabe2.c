/*gekuerzte summe zweier Brueche
Input: Zaehler und Nenner der beiden Brueche, nacheinander
Output: vollst gekuerzte summe*/
//for reasons, the program dies with the second scanf, so I could not test it

#include <stdio.h>

int main () {
    //z f zaehler, n f nenner
    int a;
    int b;
    int c;
    int d;

    //ersten bruch einlesen
    printf("Geben Sie Zaehler und Nenner des ersten Bruchs ein: \n");
    scanf("%d %d", &a, &c);
    //zweiten bruch einlesen
    printf("Geben Sie Zaehler und Nenner des zweiten Bruchs ein: \n");
    scanf("%d %d", &b, &d);
    

    /*Summenbildung, ohne nachdenken*/
    int z_sum;
    int n_sum;
    z_sum = a * d + b * c;
    n_sum = c * d;

    /*negativitaet beruecksichtigen*/
    int vz = 1;
    if (z_sum < 0){
        if (n_sum > 0) {
            vz = -1;
            z_sum = vz*z_sum;
        }
        else {
            z_sum = vz*z_sum;
            n_sum = vz*z_sum;
        }
    }
    else {
        if (n_sum < 0) {
            vz = -1;
            n_sum = vz*n_sum;
        }
    }
    


    /*kuerzen, dazu gcd von zaehler u nenner aus beiden rausteilen,
    nutze dafuer euclid aus aufgabe 1*/
    int ggt;
    int z_sum_orig;
    /*vertausche a und b falls b <= a*/
    if (z_sum <= n_sum) {
        int temp;
        temp = n_sum;
        n_sum = z_sum;
        z_sum = temp;
        z_sum_orig = z_sum;
    }

    /*schleife bis a kleiner eins wird*/
    while (n_sum >= 1) {
        /*check if b divides a*/
        if (z_sum % n_sum == 0) {
            ggt = n_sum;
            break;
        }
        z_sum = z_sum  % n_sum;

    }
    z_sum = z_sum / ggt;
    n_sum = n_sum / ggt;

    /*jetzt noch vorzeichen wieder dazu, zweimal minus wird gekuerzt, minus
    kommt immer in den zaehler*/
    z_sum = vz * z_sum;

    /*Ausgabe*/
    printf("Gekuerzte Summe aus Eingabezahlen ist %d %d",z_sum,n_sum);
}