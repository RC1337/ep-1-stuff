/*monte carlo simulation von pi
Input: nat zahl N
Output: approximiertes pi*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main () {
    srand(time(NULL));
    int N;
    int t;
    t = 0;
    printf("Geben Sie eine natuerliche Zahl N ein : \n");
    scanf("%d",&N);

    //Erzeugen der Zufallspunkte
    /*nutzen rand()-funktion und teilen durch maximalwert*/
    for (int i = 0; i<N; i++){
        double x;
        double y;
        double max;
        max = RAND_MAX;
        x = rand()/max;
        y = rand()/max;
        if (x*x+y*y <= 1){
            t = t + 1;
        }
    }
    //finale berechnung
    double appr;
    appr = 4*t/N;
    printf("Approximation zur geg Punktzahl ergibt pi = %f \n",appr);

    return 0;

}