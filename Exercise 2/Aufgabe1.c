//euclidian algorithm for gcd
/*Input: zwei nat zahlen a,b
output: ggt*/

#include <stdio.h>

int main () {

int a;
int b;
int ggt;
printf("Geben Sie zwei natuerliche Zahlen ein: ");
scanf("%d %d", &a, &b);
int b_orig;
b_orig = b;

/*vertausche a und b falls b <= a*/
if (b <= a) {
    int temp;
    temp = a;
    a = b;
    b = temp;
    b_orig = b;
}

/*schleife bis a kleiner eins wird*/
while (a >= 1) {
    /*check if b divides a*/
    if (b % a == 0) {
        ggt = a;
        break;
    }
    b = b  % a;

}
printf("ggt von %d und %d ist : %d", a, b_orig,ggt);



}