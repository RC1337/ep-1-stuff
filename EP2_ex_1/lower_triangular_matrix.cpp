#include "lower_triangular_matrix.hpp"
#include <iostream>
#include <exception>
#include <stdexcept>

/*
Johannes Bogon 2835480
Michael Lies 2669805

Deallocate wirft einen Fehler aus, immer wenn die letzte Zeile gelöscht werden soll. An der
Größe spielen bringt nichts, in Aufgabe 3 funktioniert derselbe Ansatz ohne Probleme. Kommt
da irgendwas mit den Adressen durcheinander, oder gibt es einen anderen Grund dafür?

*/

void allocate(LTmatrix* m, const unsigned int size) {
  m->n = size;
  m->data = new double*[size];
  for (unsigned int i=0; i<size;i++) { //n Zeilen, aber speichern nur bis zur Diagonalen
    m->data[i] = new double[i];
  }

}

void deallocate(LTmatrix* m) {
  if (m->data != nullptr) {
    //also wenn was drin ist
    for (unsigned int i=0; i<m->n;i++) {
      delete[] m->data[i];
    }
    delete[] m->data;
    m->data = nullptr;
    m->n = 0;
  }

}

void copy(const LTmatrix* src, LTmatrix* dest) {
  //nach Größengleichheit schauen
  if (src->n != dest->n) {
    deallocate(dest);
    allocate(dest,src->n);
  }
  for (unsigned int i=0;i<src->n;i++) {
    for (unsigned int j = 0;j<=i;j++) {
      dest->data[i][j] = src->data[i][j];
    }
  }
}

void print(const LTmatrix* src) {
  if (src->n == 0) {
    std::cout << "[leere Matrix]" << std::endl;
  }
  else {
    for (unsigned int i=0;i<src->n;i++) {
      for (unsigned int j = 0;j<=i;j++) {
        std::cout << src->data[i][j] << " ";
      }
      std::cout << std::endl;
  }
  }
}

void add(const LTmatrix* src1, const LTmatrix* src2, LTmatrix* dest) {
  //TODO: Implement function add
  if (src1->n != src2->n) {
    throw std::logic_error("Matrizen haben unterschiedliche Größe!");
  }
  else {
    //ggf Größe anpassen
    if (dest->n != src1->n) {
      deallocate(dest);
      allocate(dest,src1->n);
    }
    //jetzt tatsächlich addieren
    for (unsigned int i=0;i<src1->n;i++) {
      for (unsigned int j = 0;j<=i;j++) {
        double add = src1->data[i][j] + src2->data[i][j];
        dest->data[i][j] = add;
      }
    }
  }
}

void LTmult(const LTmatrix* src1, const LTmatrix* src2, LTmatrix* dest) {
  if (src1->n != src2->n) {
    throw std::logic_error("Matrizen haben unterschiedliche Größe!");
  }
  else {
    //ggf Größe anpassen
    if (dest->n != src1->n) {
      deallocate(dest);
      allocate(dest,src1->n);
    }
    //Multiplikation Dreiecksmatrix:
    //wir brauchen Zwischenspeichermatrix, falls dest eine der Multis ist
    LTmatrix temp;
    allocate(&temp,src1->n); 
    for (unsigned int i=0;i<src1->n;i++) {
      for (unsigned int j=0;j<=i;j++) {
        temp.data[i][j] = 0;
        for (unsigned int k=0;k<src1->n;k++) {
          temp.data[i][j] += src1->data[i][k]*src2->data[k][j];
        }
      }
    }
    for (unsigned int mm=0;mm<src1->n;mm++) {
      for (unsigned int nn=0;nn<mm;nn++) {
        dest->data[mm][nn] = temp.data[mm][nn];
      }
    }
  }
}

int main(void) {
  //TODO: Implement function test_run
  // You may use the following code to test your implementation:
  
  LTmatrix m1,m2,m3;
  allocate(&m1,5);
  for(unsigned int i=0; i<5; i++)
    m1.data[i][i] = i+1;
  copy(&m1,&m2);
  add(&m1,&m2,&m3);
  std::cout << "m1 = " << std::endl; print(&m1);
  std::cout << "m2 = " << std::endl; print(&m2);
  std::cout << "m3 = " << std::endl; print(&m3);
  for(unsigned int i=0; i<5; i++)
    for(unsigned int j=0; j<=i; j++)
      m1.data[i][j]=1;
  LTmult(&m3,&m1,&m3);
  std::cout << "m1 = " << std::endl; print(&m1);
  std::cout << "m3 = " << std::endl; print(&m3);
  deallocate(&m1); 
  deallocate(&m2); 
  deallocate(&m3);
  
}
