#include "triangular_matrix_class.hpp"
#include <iostream>
#include <exception>
#include <stdexcept>

LTmatrix::LTmatrix(const unsigned int s) {
  size = s;
  data = new double* [size];
  for (unsigned int i=0; i<size;i++) {
    data[i] = new double[i];
  }
  for (unsigned int j=0; j<size;j++) {
    for (unsigned int k=0; k<j;k++) {
      data[j][k] = 0;
    }
  }
}


LTmatrix::LTmatrix(const LTmatrix& other) {
  size = other.size;
  data = new double* [size];
  for (unsigned int i=0; i<size;i++) {
    data[i] = new double[i];
  }
  for (unsigned int j=0; j<size;j++) {
    for (unsigned int k=0; k<j;k++) {
      data[j][k] = other.data[j][k];
    }
  }
}

LTmatrix::~LTmatrix() {
  if (data != nullptr) {
    for (unsigned int i=0; i<size;i++) {
      delete[] data[i];
    }
    delete[] data;
    data = nullptr;
    size = 0;
  }
}

void LTmatrix::set(const unsigned int i, const unsigned int j, const double value) {
  if (j > i) {
    throw std::out_of_range("Element ist nicht im unteren Dreiecksteil");
  }
  else {
    if (i >= size) {
      throw std::out_of_range("Element ist außerhalb der Matrixgröße");
    }
    else {
      data[i][j] = value;
    }
    }
}

double LTmatrix::get(const unsigned int i, const unsigned int j) const {
  double val;
  if (i >= size) {
    throw std::out_of_range("Element ist außerhalb der Matrixgröße");
  }
  else {
    if (j > i) {
      val = 0;
    }
    else {
      val = data[i][j];
    }
  }
  return val;
}

void LTmatrix::copy_from(const LTmatrix& src) {
  if (size != src.size) {
    size = src.size;
    data = new double* [size]; //not sure if should do something to the old data pointer
    for (unsigned int i=0; i<size;i++) {
      data[i] = new double[i];
    }
    for (unsigned int j=0; j<size;j++) {
      for (unsigned int k=0; k<j;k++) {
        data[j][k] = 0;
      }
    }
  }
  for (unsigned int i=0;i<size;i++) {
    for (unsigned int j=0;j<size;j++) {
      data[i][j] = src.data[i][j];
    } 
  }
}


void LTmatrix::add(const LTmatrix& src1, const LTmatrix& src2) {
  if (src1.size != src2.size) {
    throw std::logic_error("Matrizen sind nicht gleich groß");
  }
  else {
    if (size != src1.size) { //ggf size anpassen
      size = src1.size;
      data = new double* [size]; //not sure if should do something to the old data pointer
      for (unsigned int i=0; i<size;i++) {
        data[i] = new double[i];
      }
      for (unsigned int j=0; j<size;j++) {
        for (unsigned int k=0; k<j;k++) {
          data[j][k] = 0;
        }
      }
    }
    //tatsächliches addieren
    for (unsigned int i=0;i<size;i++) {
      for (unsigned int j = 0;j<=i;j++) {
        double add = src1.data[i][j] + src2.data[i][j];
        data[i][j] = add;
      }
    }
  }
}

void LTmatrix::mult(const LTmatrix& src1, const LTmatrix& src2) {
  if (src1.size != src2.size) {
    throw std::logic_error("Matrizen sind nicht gleich groß");
  }
  else {
    if (size != src1.size) { //ggf size anpassen; note: wenn ich hierhin komm, ist es keine src
      size = src1.size;
      data = new double* [size]; //not sure if should do something to the old data pointer
      for (unsigned int i=0; i<size;i++) {
        data[i] = new double[i];
      }
      for (unsigned int j=0; j<size;j++) {
        for (unsigned int k=0; k<j;k++) {
          data[j][k] = 0;
        }
      }
    }
    //multiplizieren, via Umweg über temp matrix
    LTmatrix temp = LTmatrix(size);
    for (unsigned int i=0;i<size;i++) {
      for (unsigned int j=0;j<=i;j++) {
        temp.data[i][j] = 0;
        for (unsigned int k=0;k<size;k++) {
          temp.data[i][j] += src1.data[i][k]*src2.data[k][j];
        }
      }
    }
    //und das jetzt in uns kopieren
    for (unsigned int l = 0; l<size;l++) {
      for (unsigned int m = 0;m<=l;m++) {
        data[l][m] = temp.get(l,m);
      }
    }
  }
}

LTmatrix& LTmatrix::operator=(const LTmatrix& other) {
  (*this).copy_from(other);
  return (*this);
}


LTmatrix LTmatrix::operator+(const LTmatrix& other) const {
  LTmatrix sum = LTmatrix(0); //größe passt sich dann eh an
  sum.add((*this),other);
  return sum;
}

LTmatrix LTmatrix::operator*(const LTmatrix &other) const {
  LTmatrix prod = LTmatrix(0);
  prod.mult((*this),other);
  return prod;
}

int main(void) {
  //test für erzeugen + zerstören
  LTmatrix testm = LTmatrix(2);
  //test zum setten und getten
  std::cout <<"Matrix zum Testen gemacht: testm, [0 0;0 0]" << std::endl;
  std::cout << "alter Wert bei 1,0 : " << testm.get(1,0) << std::endl;
  testm.set(1,0,4.5);
  std::cout << "neuer Wert bei 1,0: " << testm.get(1,0) << std::endl;
  //testm.get(2,1); //um abort zu testen
  //test für copy
  LTmatrix cop = LTmatrix(3);
  cop.set(1,0,7);
  std::cout << "cop = 3x3 Matrix mit einer sieben auf 1,0 erstellt" << std::endl;
  testm.copy_from(cop);
  std::cout << "neue Matrix in tesm kopiert, jetzt Wert von 1,0 : "<< testm.get(1,0) << std::endl;
  LTmatrix add1 = LTmatrix(2);
  add1.set(1,0,1);
  std::cout << "Matrix add1 = [0 0 ; 1 0] erstellt" << std::endl;
  LTmatrix add2 = LTmatrix(2);
  add2.set(0,0,1);
  add2.set(1,0,1);
  std::cout << "Matrix add2 = [1 0; 1 0] erstellt" << std::endl;
  testm.add(add1,add2);
  std::cout <<"Wert von testm = add1 + add2 an Stelle 1,0: " << testm.get(1,0) << std::endl;
  testm.set(0,0,2);
  testm.mult(testm,testm);
  std::cout << "Setze Wert von testm an 0,0 zu 2, multipliziere testm mit sich selbst";
  std::cout << " Wert an Stelle 0,0: " << testm.get(0,0) << std::endl;
  std::cout << "Tests für operatorüberschreibungen: " << std::endl;
  testm = cop;
  std::cout << "Zuweisung von cop in testm, Wert an 1,0 : " << testm.get(1,0) << std::endl;
  LTmatrix summe = add1 + add2;
  std::cout << "Summe von add1 und add2 über + , Wert an 1,0: " << summe.get(1,0) << std::endl;
  LTmatrix prod = add1 * add2;
  std::cout << "Produkt von add1 und add2 über *, Wert an 1,0: " << prod.get(1,0) << std::endl;
}
