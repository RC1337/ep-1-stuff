#include <cmath>      // for std::sqrt, std::fabs
#include <algorithm>  // for std::max
#include <cassert>
#include <iostream>
#include <iomanip>
#include <stdexcept>

/*
Johannes Bogon 2835480
Michael Lies 2669805

Kommentar: bei kleinen k, also wo die NST nicht so weit auseinander liegen, ist die Produktmethode
schlechter. Mit der normalen p- q kommt man da auf einen null-fehler, beim Produkt geht anscheinend
etwas beim Runden verloren (macht Sinn, wenn man durch kleines Zeug teilt). Bei großen k und damit
großen Unterschieden der NST bleibt sich der Fehler bei beiden gleich.
*/


void solve_quad(double p, double q, double* x_1, double* x_2) {
  //TODO: Implement function solve_quad
  double radi = std::pow(p,2)*0.25 - q;

  if (radi < 0) {
    throw std::invalid_argument("Negative Zahl unter Wurzel - kann nicht komplex");
  }
  else {
    radi = std::sqrt(radi);
    double val_1 = -p/2 + radi;
    double val_2 = -p/2 - radi;
    *x_1 = val_1;
    *x_2 = val_2;
  }
}

void solve_quad_prod(double p, double q, double* x_1, double* x_2) {
  //erstmal selber ansatz wie für solve_quad weitgehend
  if (p == 0 && q == 0) { //null doppelte NST
    double val = 0;
    *x_1 = val;
    *x_2 = val;
  }
  else {
    double radi = std::pow(p,2)*0.25 - q;
    if (radi < 0) {
      throw std::invalid_argument("Negative Zahl unter Wurzel - kann nicht komplex");
    }
    else {
      if (p<0) {
        //p kleiner Null, finde da die größere NST
        double val_1 = p/2 + radi;
        double val_2 = q/val_1;
        *x_1 = val_1;
        *x_2 = val_2;
      }
      else {
        //jetzt muss p größer gleich null sein (und bei gleich null q ungleich null)
        double val_2 = p/2 - radi;
        double val_1 = q/val_2;
        *x_1 = val_1;
        *x_2 = val_2;
      }
    }
  }
}

int main(void) {
  //setup
  std::cout << std::scientific;
  double x_1_q=0;
  double x_2_q = 0;
  double x_1_p = 0;
  double x_2_p = 0;
  double x1 = 1;
  double x2 = 1;
  double akt_abw_p = 0;
  double akt_abw_q = 0;
  double p;
  double q;
  std::cout << std::setw(5) << "k" << std::setw(16) << "NST p-q" << std::setw(16) <<"Abw p-q"; 
  std::cout <<std::setw(16) << "NST prod" << std::setw(16) << "Abw prod" << std::endl;

  //k-Schleife
  for (int k = 0; k<=25;k++){
    x1 = std::pow(10,k);
    p = (-1)*pow(10,k)-1;
    q = pow(10,k);
        std::cout << q << std::endl;
    //berechne NST über p-q
    solve_quad(p,q,&x_1_q,&x_2_q);
    solve_quad_prod(p,q,&x_1_p,&x_2_p); 
    akt_abw_q = std::max(x1-x_1_q,x2-x_2_q);
    akt_abw_p = std::max(x1-x_1_p,x2-x_2_p);
    std::cout << std::setw(5) << k << std::setw(16) << x_1_q << std::setw(16) << akt_abw_q;  
    std::cout <<std::setw(16) << x_1_p << std::setw(16) << akt_abw_p << std::endl;
    std::cout << std::setw(5) << " " << std::setw(16) << x_2_q << std::setw(16) << " ";  
    std::cout <<std::setw(16) << x_2_p << std::setw(16) << " " << std::endl;
    
  } 

  return 0;
    
  
}
