#include <stdio.h>
#include <stdlib.h>
#include <math.h>

typedef struct player{
  char name[30];
    float rating;
  int games_played;
}player;

float predict(player* A, player* B){
	float RA = A->rating; 
  float RB = B->rating;
  float EA;
  float interm;

  interm = (RB-RA)/400;

  EA = 1/(1+powf(10,interm));

  return EA;


}

void update(player* A, player* B, double result){
	//erstmal für beide die Spielanzahl um eins hoch
  A->games_played += 1;
  B->games_played += 1;
  int k_A, k_B;
  //erwartetes Ergebnis
  float E_A = predict(A,B);
  float E_B = 1 - E_A; //es gibt genau einen Punkt zu verteilen
  //neue elo
  if (A->games_played <= 30) {
    k_A = 40;
  }
  else {
    k_A = 20;
  }
  if (B->games_played <= 30) {
    k_B = 40;
  }
  else {
    k_B = 20;
  }
  A->rating = A->rating + k_A*(result - E_A);
  B->rating = B->rating + k_B*(1-result -E_A);

}

int main(void) {
  player A = {"Magnus Carlsen",2855, 25};
  player B = {"Jan Nepomnjaschtschi", 2782, 28};
  update(&A,&B,0.5);  //1
  update(&A,&B,0.5);  //2
  update(&A,&B,0.5);  //3
  update(&A,&B,0.5);  //4
  update(&A,&B,0.5);  //5
  update(&A,&B,1);  //6
  update(&A,&B,0.5);  //7
  update(&A,&B,1);  //8
  update(&A,&B,1);  //9
  update(&A,&B,0.5);  //10
  update(&B,&A,0);  //11
  // In reality players with consistent rating above 2400 use a k-value of 10.
  printf("rating after 11 games:\n");
  printf("%s: %i \n", A.name, (int)A.rating);
  printf("%s: %i \n", B.name, (int)B.rating);
}
