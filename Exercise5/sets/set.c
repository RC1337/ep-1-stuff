#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "set.h"

//typedef struct node{
//	int data;
//	struct node * next;
//}node;

node * new_node(int data) {
	node * e = (node *)malloc(sizeof(node));
	e->data =data;
	e->next = NULL;
	return e;
}

void free_node(node * node){
	free(node);
}

void set_insert(node** head, int data){
	node *cur = *head;
	node *previous = NULL;
	node *new = new_node(data);
	int inserted = 0;
	//enthält liste schon daten?
	//nein: dann starte direkt mit unserem node
	if (cur == NULL){
		*head = new;
	}
	//sonst
	else {
		//special case: erster node is schon größer
		//dann einfach vorne an liste dranhängen
		if (data < cur->data) {
			new->next = *head;
			*head = new;
		}
		else {
			//sonst: durch die nodes durch, bis wir einen finden, der größer ist als data
			//können dabei nutzen dass liste sortiert ist - das erste was wir finden ist dann schon
			//das richtige
			while (cur->next != NULL) {
				//soll jedes elt nur max einmal geben, also wenns den schon gibt, aufhören
				if (data == cur->data) {
					inserted = 1;
					break;
				}
				else {
					//wenn data kleiner ist, haben wir das richtige element gefunden
					if (data < cur->data) {
						previous->next = new;
						new->next = cur;
						inserted = 1;
						break;
					}
				}
				previous = cur;
				cur = cur->next;
			}

			//wir sind am listenende angekommen, ohne dass wir kleiner waren
			//sind wir kleiner oder größer als letztes listenelement?
			if (inserted == 0) {
				if (data < cur->data) {
					//kleiner, also vor letztem element einfügen
					previous->next = new;
					new->next = cur;
				}
				else {
					if (data > cur->data) {
						//größer, also ans ende hängen
						cur->next = new;
						new->next = NULL;
					}
				}
			}

		}
	}

}

void set_delete (node ** head, int data){
	node *cur = *head;
	node *previous = NULL;
	//hat die liste überhaupt was zum löschen?
	if (cur != NULL) {
		//ist head schon das, wo wir löschen wollen?
		if (cur->data == data) {
			//free(head); //da bin ich mir nicht ganz sicher - lösche ich jetzt die daten und passt das so
			*head = cur->next;
		}
		//ansonsten: weiter durch die liste schauen
		else {
			while (cur != NULL) {
				if (cur->data == data) {
					previous->next = cur->next;
					free(cur);
					break; //kann abbrechen, weil gibt jedes elt nur max einmal
				}
				previous = cur;
				cur = cur->next;
			}
		}
	}
}

void set_print(node * head){
	node * cur = head;
	while (cur != NULL) {
		printf("%i\n", cur->data);
		cur = cur->next;
	}
}

int set_count(node * head){
	node * cur = head;
	int count = 0;
	while (cur != NULL) {
		count++;
		cur = cur->next;
	}
	return count;
}

node* set_union(node * head1, node * head2) {
	node * newhead;
	node * tobeadded;
	//welcher ist der neue head? bzw ist überhaupt irgendwas in den listen?
	if (head1 == NULL && head2 == NULL) {
		newhead = NULL;
	}
	else{ 
		if (head1 == NULL) {
			newhead = head2; //dann brauchen wir auch nichts weiter machen - vereinigte liste
			//kann nur liste2 sein
		}
		else{
			if (head2 == NULL) {
				newhead = head1; //ebenso
			}
			else {//jetzt muss tats. sortiert werden
				if (head1->data < head2->data) {
					newhead = head1;
					tobeadded = head2;
				}
				else {
					newhead = head2;
					tobeadded = head1;
				}
				while (tobeadded != NULL) {
					set_insert(&newhead,tobeadded->data);
					tobeadded = tobeadded->next;
				}
			}
		}
	}

	return newhead;

}
