#include <stdlib.h>
#include <stdio.h>

#include "mergesort.h"

//Input: arr is an array of length at least r, for which the subarrays arr[l],..,arr[m] and arr[m+1],..,arr[r] are sorted.
//Merges the two subarrays and writes the results to arr[l],..,arr[r]. 
void merge(int l, int m, int r, int* arr){
 
    int i, j, k; 
    int n1 = m - l + 1; 
    int n2 = r - m;   
    
    int L[n1], R[n2]; 

    for (i = 0; i < n1; i++) 
        L[i] = arr[l + i]; 
    for (j = 0; j < n2; j++) 
        R[j] = arr[m + 1 + j]; 
  
    
    i = 0;  
    j = 0; 

    k = l; 
    while (i < n1 && j < n2) { 
        if (L[i] <= R[j]) { 
            arr[k] = L[i]; 
            i++; 
        } 
        else { 
            arr[k] = R[j]; 
            j++; 
        } 
        k++; 
    } 
  
    while (i < n1) { 
        arr[k] = L[i]; 
        i++; 
        k++; 
    } 
  
    while (j < n2) { 
        arr[k] = R[j]; 
        j++; 
        k++; 
    } 
}

//Input:arr is an integer arrays of length n. 
//Sorts the subarray of arr between indices l and r in ascending order.
void mergesort(int n, int l, int r, int* arr) {
 
    if (l < r) { 
        int m = l + (r - l) / 2; 

        mergesort(n,l,m,arr); 
        mergesort(n, m + 1, r, arr); 
  
        merge(l, m, r, arr); 
    } 

  
}	
