/*
 ============================================================================
 Name        : bin_search.c
 Author      : mglaeser
 Description : Musterlösung für Aufgabe 4.1, berechnet sqrt(2)
 ============================================================================
 */

#include "zero.h"
// eps is 1E-9

double zero(double(*f)(double),double a, double b){

    double m;
    //sanity checks
    if (b < a) {
        printf("b ist kleiner als a, macht keinen Sinn\n");
        return 0;
    }
    else {
        if ((f(a) < 0 && f(b) < 0) || (f(a) > 0 && f(b) > 0)) {
            printf("f(a) und f(b) haben gleiches Vorzeichen, kann keine NST berechnen\n");
            return 0;
        }
        else {
            //jetzt können wir anfangen mit arbeiten
            while (a < b) {
                if (f(a) == 0) {
                    printf("%f ist NST von f\n", a);
                    return a;
                    break;
                }
                if (f(b) == 0) {
                    printf("%f ist NST von f\n",b);
                    return b;
                    break;
                }
                //note: wenn a und b beide NST sind, geben wir nur a als NST ab, schlimm?
                else {
                    if (b-a < eps) {
                        //toleranzgrenze gerissen
                        printf("%f ist im Rahmen der Toleranz NST von f\n",a);
                        return a;
                        break;
                    }
                    else {
                        m = (a+b)/2;
                        if ((f(a) > 0 && f(m) <= 0) || (f(a)<0 && f(m) >= 0)) {
                            b = m;
                        }
                        else {
                            //nehmen an, dass kein mist passiert, checken daher nicht vz von b und m
                            a = m;
                        }
                    }
                }
                
            }
            printf("ohne Ergebnis durchgelaufen, kann nichts finden, auch keine Fehler, komisch!\n");
            return 0;
        }
    }
}