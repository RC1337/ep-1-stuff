#ifndef BINSEARCH
#define BINSEARCH

#include <stdio.h>
#include <stdlib.h>
#define eps 1E-9

double zero(double(*f)(double),double a, double b);

#endif /* BINSEARCH */
