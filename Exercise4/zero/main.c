#include <stdio.h>
#include <stdlib.h>
#include "zero.h"
#include <math.h>


double f(double x){
	return 2 -(x*x);
}

int main(void) {
	printf("The square root of 2 is %f:\n", zero(f,1,2));
	return 0;
}
