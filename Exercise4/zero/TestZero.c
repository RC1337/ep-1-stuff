
#include "zero.h"
#include "unity.h"
#define delta 1E-6


void setUp(void)
{
}

void tearDown(void)
{
}

double f(double x){
	return 2-x*x;
}


double g(double x){
	return 3*x*x*x + 7*x*x -18*x+3;
}

double h(double x){
	return 4 - 1/(x*x);
}

double k(double x) {
	return x*x + 2;
}

void test_zero_1(void)
{
  	TEST_ASSERT_FLOAT_WITHIN(delta, 1.414214, zero(f,1,2));
}

void test_zero_2(void)
{
  	TEST_ASSERT_FLOAT_WITHIN(delta, -3.926341, zero(g,-5,-3));
}


void test_zero_3(void)
{
  	TEST_ASSERT_FLOAT_WITHIN(delta, 0.5, zero(h,0.3,1));
}

void test_zero_4(void)
{

	TEST_ASSERT_FLOAT_WITHIN(delta, 0, zero(k,0.3,1));
  	//TEST_FAIL_MESSAGE("Test not yet implemented.");
}

void test_zero_5(void)
{
	TEST_ASSERT_FLOAT_WITHIN(delta, 0, zero(h,2,1));
  	//TEST_FAIL_MESSAGE("Test not yet implemented.");
}
