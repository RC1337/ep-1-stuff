#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#include "print_maze.h"
#include "random_direction.h"



/* Chooses a direction at random using the function random_direction() 
 * and checks if it is a valid cell for visiting 
 * If so, it recursively calls the same function with the new coordinates 
 */
void dfs_maze(char **mat, int m, int n, int x, int y){
	
	//soll tun: nimmt eine der unbesuchten Nachbarzellen dazu und erweitert Labyrinth drum
	//ruft sich dann wieder auf: erweitertes Labyrinth als starter, neue Zelle als Startzelle
	//wir sind in Zelle x,y, also im char an der Stelle [2i+1,2j+1]
	//überprüfe ob wir noch unbesuchte Nachbarzellen haben, bei der ersten davon: füg sie dazu

	/*
	als Pseudo-code:
	schau alle vier möglichen Wände der zelle x,y an: links, rechts, oben, unten
	bei der ersten davon, die tatsächlich eine Wand, also noch unbesucht ist (und nicht der Rand
	des Labyrinths: besuch diese)
	falls keine davon unbesucht (oder keine Wand): mach einen Schritt zurück und versuch es von da
	cave: existierende Zellen muss man auch iwie markieren - es dürfte sinniger sein, x,y direkt als
	position im gitter zu übergeben - was in der main auch passiert
	Nachbarzellen haben Abstand 2 - Übergang also durch +2 in Schrittrichtung
	*/


	
	print_maze(mat,2*m+1,2*n+1);
	printf("\n \n");

	//mach mir random nummern um reihenfolge zu entscheiden
	int a0[4] = {1,2,3,4};
		int a1[3];
	int a2[2];
	int a_randomised[4];
	int index1 = rand() %3;
		int looper;
	int ind_a1 = 0;
	for (looper = 0;looper<4;looper++)
	{
		if (looper == index1) {
			a_randomised[0] = a0[looper];
		}
		else {
			a1[ind_a1] = a0[looper];
			ind_a1++;

		}
	}
	int index2 = rand() %2;
		int ind_a2 = 0;
	for (looper = 0;looper<3;looper++)
	{
		if (looper == index2) {
			a_randomised[1] = a1[looper];
		}
		else {
			a2[ind_a2] = a1[looper];
			ind_a2++;
		}
	}
	int index3 = rand()%1;
	for (looper=0;looper<2;looper++) {
		if (looper == index3) {
			a_randomised[2] = a2[looper];
		}
		else {
			a_randomised[3] = a2[looper];
		}
	}
	
	for (int k = 0;k<4;k++) {
		int a = a_randomised[k];
		switch(a) {
			case 1: //Wand nach oben existiert
				if (x>1 && mat[x-1][y] == 1) {
					if (mat[x-2][y] == 1) {
						mat[x-1][y] = 0;
						mat[x-2][y] = 0;
						dfs_maze(mat,m,n,x-2,y);
					}
				}
			case 2: //Wand nach unten existiert
				if (x<2*m-1 && mat[x+1][y] == 1) {
					if (mat[x+2][y]==1) {
						mat[x+1][y] = 0;
						mat[x+2][y] = 0;
						dfs_maze(mat,m,n,x+2,y);
					}
				}
			case 3: //Wand nach links existiert
				if (y > 1 && mat[x][y-1] == 1) {
					if (mat[x][y-2] == 1) {
						mat[x][y-1] = 0;
						mat[x][y-2] = 0;
						dfs_maze(mat,m,n,x,y-2);
					}
				}
			case 4: //Wand nach rechts existiert
				if  (y < 2*n-1 && mat[x][y+1] == 1) {
					if (mat[x][y+2]==1) {
						mat[x][y+1] = 0;
						mat[x][y+2] = 0;
						dfs_maze(mat,m,n,x,y+2);
					}
				}
		}
	}
	
	

	/*
	//Wand nach oben existiert
	if (x>1 && mat[x-1][y] == 1) {
		if (mat[x-2][y] == 1) {
			mat[x-1][y] = 0;
			mat[x-2][y] = 0;
			dfs_maze(mat,m,n,x-2,y);
		}
	}
	//Wand nach unten existiert
	if (x<2*m-1 && mat[x+1][y] == 1) {
		if (mat[x+2][y]==1) {
			mat[x+1][y] = 0;
			mat[x+2][y] = 0;
			dfs_maze(mat,m,n,x+2,y);
		}
	}
	//Wand nach links existiert
	if (y > 1 && mat[x][y-1] == 1) {
		if (mat[x][y-2] == 1) {
			mat[x][y-1] = 0;
			mat[x][y-2] = 0;
			dfs_maze(mat,m,n,x,y-2);
		}
	}
	//Wand nach rechts existiert
	if  (y < 2*n-1 && mat[x][y+1] == 1) {
		if (mat[x][y+2]==1) {
			mat[x][y+1] = 0;
			mat[x][y+2] = 0;
			dfs_maze(mat,m,n,x,y+2);
		}
	}*/	

}


int main ()
{
	int i, j, rows, cols, start_x, start_y, m, n, x, y;
	char **maze;

	srand(time(0));
	
	printf("Insert the dimensions of the maze and the starting location (start location (0,0) - (m-1, n-1) \n");
	scanf("%d %d %d %d", &m, &n, &x, &y);

	while((m <= 0) || (n <= 0) || (x >= m) || (y >= n) || (x < 0) || (y < 0))
	{
		printf("Wrong input. Insert positive integers for the size of the maze and start location values between (0,0) and (m-1, n-1) \n");
		scanf("%d %d %d %d", &m, &n, &x, &y);
	}
	
	rows = 2 * m + 1;
	cols = 2 * n + 1;
	//printf("rows = %d , cols = %d \n", rows, cols);
	start_x = 2 * x + 1;
	start_y = 2 * y + 1;
	
	maze = malloc (rows * sizeof (char *));
	if (!maze) {
		printf("Error! Memory not allocated!");
		exit(0);
	}

	for (i=0;i<rows;i++) {
		maze [i] = malloc (cols * sizeof (char));
		if (! maze[i]) {
			printf("Error, memory not allocated!");
			exit(0);
		}
	}

	//initialisieren: wir haben noch nichts besucht, also haben alle beliebigen wände wert 1
	for (i=0;i<rows;i++){
		for (j=0;j<cols;j++) {
			maze[i][j] = 1;
		}
	}
	maze[start_x][start_y] = 0;

	dfs_maze(maze,m,n,start_x,start_y);

	return 0;
  }
