#ifndef __TRIANGLE_HPP
#define __TRIANGLE_HPP

class triangle {
public:
  triangle(void){}; // empty default constructor, here implemented
  triangle(const triangle& other);
  void setNode(const unsigned int i , const double x, const double y);
  double getArea(void) const;
  virtual double integrate(double (*)(const double, const double)) const = 0;
  
  double* getNode(const unsigned int i) {
    // a get function, already implemented
    if (i <= 2)
      return node[i];
    else
      return nullptr;
  }

//protected:
  double node[3][2];
};

class triangleP0 : public triangle {
public:
  triangleP0(){}; // empty default constructor, here implemented
  triangleP0(const triangle& other);
  double integrate(double (*)(const double, const double)) const;
};

class triangleP1 : public triangle {
public:
  triangleP1(){}; // empty default constructor, here implemented
  triangleP1(const triangle& other);
  double integrate(double (*)(const double, const double)) const;
};

/*
 * Function to run and test your own implementation of polynom
 */
int main(void);

#endif
