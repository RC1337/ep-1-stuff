/*
Johannes Bogon 2835480
Michael Lies 2669805

Ich habe das mit protected nicht hinbekommen; direkter Zugriff geht dann klar nicht, aber
die get-Funktion wurde nicht zugelassen. Daher aufgegeben und das node-attribut auf public gesetzt
*/

#include <iostream>
#include <cmath>
#include "triangle.hpp"

triangle::triangle(const triangle& other) {
  //man muss nur den node-vektor setzen
  (*this).setNode(0,other.node[0][0],other.node[0][1]);
  (*this).setNode(1,other.node[1][0],other.node[1][1]);
  (*this).setNode(2,other.node[2][0],other.node[2][1]);
}

void triangle::setNode(const unsigned int i, const double x, const double y) {
  switch (i) {
    case 0:
      node[0][0] = x;
      node[0][1] = y;
      break;
    case 1:
      node[1][0] = x;
      node[1][1] = y;
      break;
    case 2:
      node[2][0] = x;
      node[2][1] = y;
      break;
    default:
      throw std::logic_error("Ecke existiert nicht");
      break;
  }
}

double triangle::getArea(void) const {
  //da es keine eingebaute formel für det gibt, nehm ich an, dass man das umformen darf: Matrix ab;cd
  double a = node[1][0] - node[0][0];
  double c = node[1][1] - node[0][1];
  double b = node[2][0] - node[0][0];
  double d = node[2][1] - node[0][1];
  double val = 0.5*abs(a*d-b*c);
  return val;

}

// Implement the copy constructor for class triangleP0 here:

triangleP0::triangleP0(const triangle& other) : triangle() {
  //von der logiK: muss die daten aus other rauskriegen und hier reinpacken
  (*this).setNode(0,other.node[0][0],other.node[0][1]);
  (*this).setNode(1,other.node[1][0],other.node[1][1]);
  (*this).setNode(2,other.node[2][0],other.node[2][1]);
   
}

double triangleP0::integrate(double (*f)(const double, const double)) const {
  double T = (*this).getArea();
  double x = ((*this).node[0][0]+(*this).node[0][1]+(*this).node[0][2])/3;
  double y = (node[1][0]+node[1][1]+node[1][2])/3;
  double f_val = f(x,y);
  double val = T*f_val;
  return val;
}

// Implement the copy constructor for class triangleP1 here:
triangleP1::triangleP1(const triangle& other) : triangle() {
  (*this).setNode(0,other.node[0][0],other.node[0][1]);
  (*this).setNode(1,other.node[1][0],other.node[1][1]);
  (*this).setNode(2,other.node[2][0],other.node[2][1]);
  
}

double triangleP1::integrate(double (*f)(const double, const double)) const {
  double T = (*this).getArea();
  double x_vals[2][3];
  for (unsigned int i=0;i<2;i++) {
    x_vals[i][0] = (4*node[i][0] + node[i][1] + node[i][2])/6;
  }
  for (unsigned int i=0;i<2;i++) {
    x_vals[i][1] = (node[i][0] + 4*node[i][1] + node[i][2])/6;
  }
  for (unsigned int i=0;i<2;i++) {
    x_vals[i][2] = (node[i][0] + node[i][1] + 4*node[i][2])/6;
  }
  double f1 = f(x_vals[0][0],x_vals[1][0]);
  double f2 = f(x_vals[0][1],x_vals[1][1]);
  double f3 = f(x_vals[0][2],x_vals[1][2]);
  double val = T*(f1+f2+f3)/3;
  return val;
}


// a test fzunction for use in main()
double f_testfun(const double x, const double y) {
  return x*y;
}
int main(void) {
  //TODO: Implement your tests as desired.
  triangleP0 T0;
  T0.setNode(0, 0.0, 0.0);
  T0.setNode(1, 1.0, 0.0);
  T0.setNode(2, 0.0, 1.0);
  std::cout << "Area T0 = " << T0.getArea() << std::endl;
  triangleP1 T1(T0); // use copy constructor
  std::cout << "Area T1 = " << T1.getArea() << std::endl;
  std::cout << "Integral T0 = " << T0.integrate(&f_testfun) << std::endl;
  std::cout << "Integral T1 = " << T1.integrate(&f_testfun) << std::endl;
  std::cout << std::scientific; 
  // The exact value of the integral of x*y over the above triangle is 1/24:
  std::cout << "Error in integral T0 = " << std::fabs(T0.integrate(&f_testfun) -1.0/24.0) << std::endl;
  std::cout << "Error in integral T1 = " << std::fabs(T1.integrate(&f_testfun) -1.0/24.0) << std::endl;
  
  // should give output like
  /*
Area T0 = 0.500000
Area T1 = 0.500000
Integral T0 = 0.055556 
Integral T1 = 0.041667
Error in integral T0 = 1.388889e-02
Error in integral T1 = 0.000000e+00
  */
  return 0;
}
