/*Johannes Bogon 2835480
Michael Lies 2669805*/

#include "upperTriangularMatrix.hpp"
#include <iostream>
#include <string>
#include <exception>
#include <stdexcept>

upperTriangularMatrix::upperTriangularMatrix(const unsigned int dim) {
  //use just the normal constructor for squarematrix and claim the object is upper triang
  if (dim == 0) {
        n = 0;
        data = nullptr;
    }
    else {
        n = dim;
        //speicher allocaten
        data = new double*[n];
        for (unsigned int i = 0;i<n;i++) {
            data[i] = new double[n];
        }
        //überall entspr wert von other rein:
        for (unsigned int j = 0;j<n;j++) {
            for (unsigned int k = j;k<n;k++) { //links von diag einfach ignorieren
                data[j][k] = 0;
            }
        }
    }
}

upperTriangularMatrix::upperTriangularMatrix(const upperTriangularMatrix& other) : squareMatrix() {
  // in the above we initialise the base class object. This is good style and prevents compiler
  // warning: "base class ‘class squareMatrix’ should be explicitly initialized in the copy constructor [-Wextra]"
  //funktioniert genau wie für die square-klasse, nur mit nullen unter diagonale, nutzen also einfach das
  n = other.n;
  data = nullptr;
  if (n != 0) {
    //speicher allocaten
    data = new double*[n];
    for (unsigned int i = 0;i<n;i++) {
        data[i] = new double[n];
    }
    //überall entspr wert von other rein:
    for (unsigned int j = 0;j<n;j++) {
        for (unsigned int k = j;k<n;k++) { //links von diag ist nichts
            data[j][k] = other.data[j][k];
        }
    }
  }
}

void upperTriangularMatrix::resize(const unsigned int dim) {
  //kann exakt analog gemacht werden wie für square
  (*this).squareMatrix::resize(dim);
}

void upperTriangularMatrix::set(const unsigned int i, const unsigned int j, const double val) {
  //kann man nicht direkt übernehmen: muss sicherstellen, dass i,j nicht unter diag
  if ( (i>= n || j >=n)) {
      throw std::logic_error("außerhalb der matrixdimension");
  }
  else if (i > j && (val != 0)) {
    //unterhalb diagonale
    throw std::logic_error("unterhalb der diagonale");
  }
  else {
    data[i][j] = val;
    }
}

void upperTriangularMatrix::add(const unsigned int i, const unsigned int j, const double val) {
  //selber Ansatz: können nur oberhalb diag addieren, also anpassen
  if ( (i>= n || j >=n)) {
      throw std::logic_error("außerhalb der matrixdimension");
  }
  else if (i > j && (val != 0)) {
    //unterhalb diagonale
    throw std::logic_error("unterhalb der diagonale");
  }
  else {
    data[i][j] += val;
  }
}

double upperTriangularMatrix::operator()(const unsigned int i, const unsigned int j) const {
  double ret = -1;
  if ( (i>= n || j >=n)) {
      throw std::logic_error("außerhalb der matrixdimension");
  }
  else if ( i > j) {
    ret = 0;
  }
  else {
    ret = data[i][j];
  }
  return ret;
}

upperTriangularMatrix& upperTriangularMatrix::operator=(const upperTriangularMatrix& rhs) {
  //kann man wieder selben code nehmen wie für square-matrix
  if (rhs.n != (*this).n) {
        (*this).resize(rhs.n);
    }
    //jetzt sind die dims gleich, muss mich also nicht drum kümmern
    for (unsigned int i = 0;i<n;i++) {
        for (unsigned int j = 0;j<n;j++) {
            (*this).data[i][j] = rhs.data[i][j];
        }
    }
    return (*this);
}

upperTriangularMatrix operator+(upperTriangularMatrix lhs, const upperTriangularMatrix& rhs) {
  //friend
  if (lhs.n != rhs.n) {
        throw std::logic_error("dimensionen nicht gleich");
    }
    else {
        for (unsigned int i =0;i<lhs.n;i++) {
            for (unsigned int j=i;j<lhs.n;j++) { //links der diag ist null
                lhs.data[i][j] += rhs.data[i][j];
            }
        }
    }
    return lhs;
}

upperTriangularMatrix operator-(upperTriangularMatrix lhs, const upperTriangularMatrix& rhs) {
  //friend
  if (lhs.n != rhs.n) {
        throw std::logic_error("dimensionen nicht gleich");
    }
    else {
        for (unsigned int i =0;i<lhs.n;i++) {
            for (unsigned int j=i;j<lhs.n;j++) {
                lhs.data[i][j] -= rhs.data[i][j];
            }
        }
    }
    return lhs;
}

upperTriangularMatrix operator*(const upperTriangularMatrix& lhs, const upperTriangularMatrix& rhs) {
  //friend
  unsigned int dimen = rhs.n;
    upperTriangularMatrix* produkt = new upperTriangularMatrix(dimen);
    if (lhs.n != rhs.n) {
        throw std::logic_error("dimensionen nicht gleich");
    }
    else {
        for (unsigned int i=0;i<produkt->n;i++) {
            for (unsigned int j=i;j<produkt->n;j++) {
                for (unsigned int k=0;k<produkt->n;k++) {
                    produkt->data[i][j] += lhs.data[i][k]*rhs.data[k][j];
                }
            }
        }
    }
    return *produkt;
}

