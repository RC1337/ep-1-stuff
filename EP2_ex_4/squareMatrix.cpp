#include "squareMatrix.hpp"
#include <iostream>
#include <string>
#include <exception>
#include <stdexcept>

squareMatrix::squareMatrix(const unsigned int dim) {
    if (dim == 0) {
        n = 0;
        data = nullptr;
    }
    else {
        n = dim;
        //speicher allocaten
        data = new double*[n];
        for (unsigned int i = 0;i<n;i++) {
            data[i] = new double[n];
        }
        //überall entspr wert von 0 rein:
        for (unsigned int j = 0;j<n;j++) {
            for (unsigned int k = 0;k<n;k++) {
                data[j][k] = 0;
            }
        }
    }
}

squareMatrix::squareMatrix(const squareMatrix& other) {
    n = other.n;
    data = nullptr;
    if (n != 0) {
        //speicher allocaten
        data = new double*[n];
        for (unsigned int i = 0;i<n;i++) {
            data[i] = new double[n];
        }
        //überall entspr wert von other rein:
        for (unsigned int j = 0;j<n;j++) {
            for (unsigned int k = 0;k<n;k++) {
                data[j][k] = other.data[j][k];
            }
        }
    }
}

squareMatrix::~squareMatrix(void) {
    if (data != nullptr) {
        for (unsigned int i = 0;i<n;i++) {
            delete[] data[i];
        }
        delete[] data;
        data = nullptr;
    }
}

void squareMatrix::resize(const unsigned int dim) { 
    if (n != dim) {
        if (n!=0) {
            for (unsigned int i=0;i<n;i++) {
                delete[] data[i];
            }
            delete[] data;
            data = nullptr;
        }
        (*this).n = dim;
        (*this).data = new double*[n];
        for (unsigned int i = 0;i<n;i++) {
            (*this).data[i] = new double[n];
        }
        //überall entspr wert von 0 rein:
        for (unsigned int j = 0;j<n;j++) {
            for (unsigned int k = 0;k<n;k++) {
                (*this).data[j][k] = 0;
            }
        }
    }
}

void squareMatrix::set(const unsigned int i, const unsigned int j, const double val) {
    if ( (i>= n || j >=n)) {
        throw std::logic_error("außerhalb der matrixdimension");
    }
    else {
        data[i][j] = val;
    }
}

void squareMatrix::add(const unsigned int i, const unsigned int j, const double val) {
     if ( (i>= n || j >=n)) {
        throw std::logic_error("außerhalb der matrixdimension");
    }
    else {
        data[i][j] += val;
    }
}


double squareMatrix::operator()(const unsigned int i, const unsigned int j) const {
    double ret = -1;
    if ( (i>= n || j >=n)) {
        throw std::logic_error("außerhalb der matrixdimension");
    }
    else {
        ret = data[i][j];
    }
    return ret;
}

squareMatrix& squareMatrix::operator=(const squareMatrix& rhs) {
    if (rhs.n != (*this).n) {
        double n_old = (*this).n;
        double n_new = rhs.n;
        (*this).resize(rhs.n);
    }
    //jetzt sind die dims gleich, muss mich also nicht drum kümmern
    for (unsigned int i = 0;i<n;i++) {
        for (unsigned int j = 0;j<n;j++) {
            (*this).set(i,j,rhs.data[i][j]);
        }
    }
    return (*this);
}

squareMatrix operator+(squareMatrix lhs, const squareMatrix& rhs) {
    if (lhs.n != rhs.n) {
        throw std::logic_error("dimensionen nicht gleich");
    }
    else {
        for (unsigned int i =0;i<lhs.n;i++) {
            for (unsigned int j=0;j<lhs.n;j++) {
                lhs.add(i,j,rhs.data[i][j]);
            }
        }
    }
    return lhs;
}

squareMatrix operator-(squareMatrix lhs, const squareMatrix& rhs) {
    if (lhs.n != rhs.n) {
        throw std::logic_error("dimensionen nicht gleich");
    }
    else {
        for (unsigned int i =0;i<lhs.n;i++) {
            for (unsigned int j=0;j<lhs.n;j++) {
                lhs.data[i][j] -= rhs.data[i][j];
            }
        }
    }
    return lhs;
}

squareMatrix operator*(const squareMatrix& lhs, const squareMatrix& rhs) {
    unsigned int dimen = rhs.n;
    squareMatrix* produkt = new squareMatrix(dimen);
    if (lhs.n != rhs.n) {
        throw std::logic_error("dimensionen nicht gleich");
    }
    else {
        for (unsigned int i=0;i<dimen-1;i++) {
            for (unsigned int j=0;j<dimen;j++) {
                for (unsigned int k=0;k<dimen;k++) {
                    produkt->add(i,j,lhs.data[i][k]*rhs.data[k][j]);
                }
            }
        }
        //letzte Zeile: da können nur zwei Einträge drin sein
        double last = lhs.data[dimen-1][dimen-1]*rhs.data[dimen-1][dimen-1];
        produkt->set(dimen-1,dimen-1,last);

    }
    return *produkt;

}

std::ostream& operator<<(std::ostream& os, const squareMatrix& rhs) {
    if (rhs.data == nullptr) {
        os << "Empty matrix" << std::endl;
    }
    else {
        for (unsigned int i =0;i<rhs.n;i++) {
            for (unsigned int j=0;j<rhs.n;j++) {
                os << rhs.data[i][j] << " ";
            }
            os << std::endl;
        }
    }
    return os;
}

