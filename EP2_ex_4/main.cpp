#include <iostream>
#include <chrono>   // f"ur std::chrono
#include "squareMatrix.hpp"
#include "upperTriangularMatrix.hpp"

#define SIZE_1 5
#define SIZE_2 100

int main(void) {
//nutze nur die bereitgestellten tests
  
  squareMatrix sm1(SIZE_1), sm2;
  for(unsigned int i=0; i<SIZE_1; i++)
    for(unsigned int j=0; j<=i; j++)
      sm1.set(i,j,i+j+1);
  std::cout << "sm1=" << std::endl << sm1 << std::endl;
  sm2 = sm1*sm1 + sm1;
  std::cout << std::scientific;
  std::cout << "sm2=" << std::endl << sm2 << std::endl;

  

  upperTriangularMatrix um1(SIZE_1), um2;
  for(unsigned int i=0; i<SIZE_1; i++)
    for(unsigned int j=i; j<SIZE_1; j++)
      um1.set(i,j,i+j+1);
  std::cout << "um1=" << std::endl << um1 << std::endl;
  um2 = um1 * um1 + um1;
  std::cout << "um2=" << std::endl << um2 << std::endl;

  squareMatrix sm3 = sm2 + um2;  // gemischter Ausdruck
  std::cout << "sm3=" << std::endl << sm3 << std::endl;
  
  upperTriangularMatrix um4(SIZE_2);
  for(unsigned int i=0; i<SIZE_2; i++)
    for(unsigned int j=i; j<SIZE_2; j++)
      um4.set(i,j,1);
  squareMatrix sm4(um4); // copy-Konstruktor der Basisklasse aufgerufen 
                         // für Instanz der abgeleiteten Klasse

  // Multiplikation für Dreiecksmatrizen soll deutlich schneller als
  // für volle quadratische Matrizen sein. Hier kommt der Test dazu:
  auto sm4ClockStart = std::chrono::high_resolution_clock::now();
  sm4 = sm4 * sm4;
  auto sm4ClockElapsed = std::chrono::high_resolution_clock::now() - sm4ClockStart;
  std::cout << "Elapsed time for sm4 = sm4 * sm4 : " << std::chrono::duration_cast<std::chrono::microseconds>(sm4ClockElapsed).count() << " microseconds." << std::endl;
  
  auto um4ClockStart = std::chrono::high_resolution_clock::now();
  um4 = um4 * um4;
  auto um4ClockElapsed = std::chrono::high_resolution_clock::now() - um4ClockStart;
  std::cout << "Elapsed time for um4 = um4 * um4 : " << std::chrono::duration_cast<std::chrono::microseconds>(um4ClockElapsed).count() << " microseconds." << std::endl;
  
    return 0;
}

