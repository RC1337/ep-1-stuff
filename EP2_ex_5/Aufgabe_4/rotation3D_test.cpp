#include <cmath>
#include "vector3D.hpp"
#include "rotation3D.hpp"
#include "smallMatrix.hpp"
# define PI           3.14159265358979323846  /* pi */


int main(void) {
	//Vektoren für diverse Drehachsen und Anwenden von Rotationen
	vector3D b = vector3D();
	vector3D a = vector3D(std::make_tuple(1., 1.0, 0.0));
	vector3D c = vector3D(std::make_tuple(2.0, 3.0, 1.0));
	std::cout << "Vektor a "<<a <<", Vektor b "<< b << std::endl;
	std::cout <<"a + c: " <<  a + c<< std::endl;  //Benötigt operator+ in vector3D.cpp
	
	std::cout << "--------------------------" << std::endl;
	std::cout << "Test Fehlerhandling bei nicht-normierbarer Achse" << std::endl;
	//rotation3D rotA = rotation3D();
	rotation3D rotA(b,0.1); //man braucht noch angle, weil die methode mit nur achse nicht implementiert
	
	infoToStream(std::cout,rotA) << std::endl;

	std::cout << "--------------------------" << std::endl;
	std::cout << "Testing Matrix-Constructor (Funktioniert erst nach Einkommentieren der operatoren in smallMatrix.tpp)" << std::endl;
	 // 45-° Rotation in x-y Ebene (Achse = z-Achse)
	 
	smallMatrix<double,3u> badMat;
	badMat(0,0) = 0.5; badMat(1,1) = 0.5; 
	badMat(1,0) = -0.5; badMat(0,1) = 0.5;
	badMat = badMat* std::sqrt(2); 
	badMat(2,2) = 1;
	std::cout <<"Det of Matrix is " <<badMat.det() << std::endl;
	/*rotation3D badRot(badMat);	
	std::cout << "Rotation axis of resulting Rotation is " <<badRot.getAxis() << std::endl;*/
	
	std::cout << "--------------------------" << std::endl;
	rotation3D copyTest(c,1.2);
	std::cout << "here" << std::endl;
	rotation3D rotB(copyTest);
	//rotation3D rotB{ rotA };
	std::cout << "Copy Constructor test\n "<< copyTest << rotB << std::endl;

	std::cout << "--------------------------" << std::endl;
	std::cout << "Setter Tests" << std::endl;
	rotA.setAxis(c);
	std::cout << rotA.getAxis() << "\nNew Axis" << std::endl;

	rotA.setAngle(1.2);
	std::cout << rotA.getAngle() << "\nNew Angle" << std::endl;
	std::cout << rotA << std::endl;
	vector3D d = vector3D(std::make_tuple(1.0, 0.0, 0.0));
	vector3D e = vector3D(std::make_tuple(0.0, 1.0, 0.0));
	vector3D f = vector3D(std::make_tuple(0.0, 0.0, 1.0));
	rotation3D rotD( d, PI / 2 );
	rotation3D rotE( e, PI / 2 );
	std::cout << "--------------------------" << std::endl;
	std::cout << "Testing Composition " << std::endl;
	std::cout << "Rotation D:\n" <<rotD.getMatrix() << std::endl;
	//Einkommentieren falls Operatorüberladungen implementiert sind
	//std::cout << "--------------------------" << std::endl;
	//std::cout << "Testing op* " << std::endl;
	//std::cout << "Before Composing, chaining (result should be same as composition below)" << std::endl;
	//std::cout << rotE*rotD << std::endl;
	//std::cout << "With op*, Order of Braces matter for rounding errors" << std::endl;
	//std::cout << rotE*(rotD*f) << std::endl;
	//std::cout << (rotE*rotD)*f << std::endl;
	
	std::cout <<"Rotation E:\n"<< rotE.getMatrix() << std::endl;
	rotE.composition(rotD);
	std::cout << "After Composition:\n"<<rotE << std::endl;
	std::cout << "--------------------------" << std::endl;
	std::cout << "Testing Apply: " << std::endl;
	vector3D g = rotE.apply(f);
	std::cout << g << std::endl;

	
	
	//Einkommentieren falls Operatorüberladungen implementiert sind
	//std::cout << "--------------------------" << std::endl;
	//std::cout << "Testing op= " << std::endl;
	//badRot = rotE;
	//std::cout << badRot << std::endl;
}