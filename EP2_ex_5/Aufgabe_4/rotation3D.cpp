#include <stdexcept>
#include <cmath>
#include <sstream>
#include "vector3D.hpp"
#include "rotation3D.hpp"
#include "smallMatrix.hpp"


//Constructors
rotation3D::rotation3D(void) {
	//Rotationsmatrix auf Einheitsmatrix, Achse auf null-tupel und drehwinkel auf 0
	smallMatrix<double,3u> matr; 
	matr(0,0) = 1;
	matr(1,1) = 1;
	matr(2,2) = 1;
	this->rotMatrix = matr;
	std::tuple<double,double,double> vec (0.0,0.0,0.0);
	vector3D axs = vector3D(vec);
	this->axis = axs;
	this->setAngle(0.0);

}

rotation3D::rotation3D(const vector3D& _axis, double _angle) {
		//Todo: Implementieren	
	//Mit angle kann nichts schiefgehen, den setzen wir einfach direkt
	this->setAngle(_angle);
	vector3D axi = vector3D(_axis);
	

	try{
		// Code der möglicherweise mit throw eine Ausnahme werfen kann
		// Hier: Berechnungen zum Achse normieren und dann Rotationsmatrix erzeugen
		axi.normalize();
		this->axis = axi;
	} catch(const std::domain_error& e){
		// Wirft der Code in try{} eine Ausnahme, wird der zum Typ der Ausnahme passende catch{}-Block ausgeführt
		// In diesem Fall wird das catch{} also nur ausgeführt, wenn der Code in try{} einen std::domain_error mit throw wirft
		// Danach läuft der Code normal weiter

		//Zusätzliche Infos: 
		//Es kann hinter einem try{}-Block bei Bedarf mehrere catch-Blöcke für verschiedene Exceptions geben.
		// Mit catch(...){} fängt man alle geworfenen Ausnahmen unabhängig von ihrem Typ
		//code hier drinnen wird eh nicht ausgeführt, da bereits setAxis den domain-error fängt
		//und dann gar nichts tut
		std::cout << "Es gab einen domain-error bei der achse, wurde zu (0,0,0) gesetzt" << std::endl;

		
	}
	//in jedem Fall haben wir jetzt angle und axis und machen damit dann nach Blatt-formel die Matrix
	smallMatrix<double,3u> matr; //das ist A
	std::tuple<double,double,double> co = _axis.getCoords();
	matr(0,1) = 0 - std::get<2>(co); //-z
	matr(0,2) = std::get<1>(co); //y
	matr(1,0) = std::get<2>(co); //z
	matr(1,2) = 0 - std::get<0>(co); //-x
	matr(2,0) = 0 - std::get<1>(co); //-y
	matr(2,1) = std::get<0>(co); //x
	smallMatrix<double,3u> id; //Id
	id(0,0) = 1;
	id(1,1) = 1;
	id(2,2) = 1;
	//damit jetzt R:
	smallMatrix<double,3u> A_sq = matr.matMult(matr);
	smallMatrix<double,3u> A_1 = matr*sin(_angle);
	smallMatrix<double,3u> A_2 = A_sq*(1-cos(_angle));
	smallMatrix<double,3u> sum = A_1+A_2;
	smallMatrix<double,3u> R = id+sum;
	//Kein Check, die Formel wird als korrekt angenommen und R einfach als Rotationmatrix gesetzt
	this->rotMatrix = R;

}

//Nützliche Hilfsfunktionen zuerst implementieren
double rotation3D::computeAngle(void) const{
//Todo: Implementieren	
//Formel siehe Aufgabenstellung
//this ist orthogonal, braucht man hier nicht mehr prüfen
//trace ist Summe der Diagonalelemente, machen wir zuerst
smallMatrix<double,3u> O = this->getMatrix();
double trO = O(0,0)+O(1,1)+O(2,2);
//damit costheta aus oberer Zeile:
double costheta = (trO-1)/2;
//daraus jetzt den eigentlichen Wert von theta
double theta = acos(costheta);
return theta;
}

vector3D rotation3D::computeAxis(void) const{
	//Todo: Implementieren	
	//von Id verschieden?
	smallMatrix<double,3u> id; //Id
	smallMatrix<double,3u> O = this->getMatrix();
	id(0,0) = 1;
	id(1,1) = 1;
	id(2,2) = 1;
	smallMatrix<double,3u> diff =O.sub(id);
	vector3D res = vector3D();
	if (diff.isIdentity()) {
		std::cout << "Rotationsmatrix ist Identität, belasse Null-Achse" << std::endl;
		
	}
	else {
		//berechnen die achse auch in echt
		smallMatrix<double,3u> OT = O.transpose();
		double factor = 1/(2*sin(this->getAngle()));
		smallMatrix<double,3u> B = (O-OT)*factor;
		double a = B(2,1);
		double b = B(0,2);
		double c = B(1,0);
		std::tuple<double,double,double> co (a,b,c);
		res.setCoords(co);
	}
	return res;
}

//Rotation Constructor from given Matrix
//Vorsicht: Prüfen ob Matrix orthogonal ist!
rotation3D::rotation3D(const smallMatrix<double,3u>& _matrix){
	//Orthogonal?
	double det = _matrix.det();
	det = abs(det);
	if (det < tolerance) {
		throw std::domain_error("Nichtdiagonale Matrix kann nicht Rotmatrix sein");
	}
	else {
		double ang = this->computeAngle();
		this->setAngle(ang);
		this->setMatrix(_matrix);
		vector3D ax = this->computeAxis();
		this->setAxis(ax);
	}
}

//Copy Constructor 
rotation3D::rotation3D(const rotation3D& _rot) {
	this->rotMatrix = _rot.getMatrix();
	this->angle = _rot.getAngle();
	this->axis = _rot.getAxis();
	this->cAngle = cos(this->angle);
	this->sAngle = sin(this->angle);
}


void rotation3D::setAngle(double _angle) {
	double x,y,z;
	std::tie(x, y, z) = this->axis.getCoords(); //std::tie ermöglicht "mehrere Rückgabewerte"
	if(!((x <this->tolerance)&& (y <this->tolerance) && (z <this->tolerance))){
		this->angle = _angle;
		this->cAngle = std::cos(this->angle);
		this->sAngle = std::sin(this->angle);
		this->rotMatrix = rotation3D((this->axis),_angle).getMatrix();
	} else {
		this->angle = _angle;
		this->cAngle = std::cos(this->angle);
		this->sAngle = std::sin(this->angle);
	}
}


void rotation3D::setAxis(vector3D _axis) {
	//Beispiel für try-catch ausdrücke, damit wird durch errors das Programm nicht abgebrochen
	try {
		_axis.normalize();
		this->axis = _axis;
		this->rotMatrix = rotation3D(_axis,(this->getAngle)()).getMatrix();
	}
	catch (const std::domain_error& e) {
		std::cout << "Got a domain error in setting axis" << std::endl;
	}
}

void rotation3D::setMatrix(const smallMatrix<double,3u>& _matrix){

	try {
		rotation3D tempRot = rotation3D(_matrix);
		this->rotMatrix = tempRot.getMatrix();
		(this->setAngle)(tempRot.getAngle());
		(this->setAxis)(tempRot.computeAxis()); //!!!!! Beruht auf KORREKTER methode computeAxis() von euch
	}
	catch (const std::domain_error& e) {
		std::cout << "Not a valid rotation matrix" << std::endl;
	}
}

double rotation3D::getAngle(void) const {
	return this->angle;
}

vector3D rotation3D::getAxis(void) const {
	return (this->axis);
}

smallMatrix<double,3u> rotation3D::getMatrix(void) const{
	return this->rotMatrix;
}

void rotation3D::composition(const rotation3D& other) {
	this->setMatrix((this->rotMatrix)*other.getMatrix());	
}

vector3D rotation3D::apply(const vector3D& vec) const {
	//Need to code matrix-vector multiplication ourselves *here*. This is essentially just a design decision.
	//Doesn't make much sense in either vec3D or smallMat
	double x,y,z;
	std::tie(x,y,z) = vec.getCoords();
	double temp1[3]= {x,y,z};
	double result[3]= {0};
	for(int i = 0; i<3; i++){
		for(int j = 0; j<3; j++){
			result[i] += temp1[j]*(this->rotMatrix)(i,j);
		}
	}
	return vector3D(std::make_tuple(result[0],result[1],result[2]));
}




void rotation3D::operator=(const rotation3D& rot) {
	this->angle = rot.angle;
	//vector3D has no copy-assignment operator, but the default
	//one is fine, because it calls the copy-assignment of std::tuple, which is a proper copy
	this->axis = rot.axis;
	this->rotMatrix = rot.rotMatrix; //We use the fact that smallMatrix has a copy-assignment operator here!
	this->cAngle = rot.cAngle;
	this->sAngle = rot.sAngle;
}

void rotation3D::operator*=(const rotation3D& other)
{
	this->composition(other);
}


vector3D rotation3D::operator*(const vector3D& vec) {
	return (this->apply)(vec);
}

rotation3D rotation3D::operator*(const rotation3D& rot1) {
	rotation3D result(*this);
	result *= rot1;
	return result;
}


std::ostream& infoToStream(std::ostream& os, const rotation3D& rot) {
	os << "Angle is " << rot.angle << std::endl;
	os << "Axis is " << rot.getAxis() << std::endl;
	return os;
}

std::ostream& operator<<(std::ostream& os, const rotation3D& rot) {//Print MAtrix
	os << rot.rotMatrix << std::endl;
	return os;
}