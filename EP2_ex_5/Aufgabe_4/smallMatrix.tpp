//dim ist fix, also dimensionskompatibilität muss man nicht prüfen

template<class T, unsigned int dim>
smallMatrix<T,dim>::smallMatrix(void) {
  for (unsigned int i=0; i < dim; i++)
    for (unsigned int j=0; j < dim; j++)
      this->data[i][j] = T(0.);
}

template<class T, unsigned int dim>
smallMatrix<T,dim>::smallMatrix(const smallMatrix<T,dim>& other) {
  for (unsigned int i=0; i < dim; i++)
    for (unsigned int j=0; j < dim; j++)
      this->data[i][j] = other.data[i][j];
}

template<class T, unsigned int dim>
T& smallMatrix<T,dim>::operator()(const unsigned int i, const unsigned int j) {
  // std::cout << "smallMatrix write access" << std::endl;
  if ((i < dim) && (j < dim))
    return this->data[i][j];
  else {
    std::cerr << "Index [" << i << "," << j << "] is out of range" << std::endl;
    throw std::range_error("Index not valid.");
  }
}

template<class T, unsigned int dim>
const T& smallMatrix<T,dim>::operator()(const unsigned int i, const unsigned int j) const {
  // std::cout << "smallMatrix read access" << std::endl;
  if ((i < dim) && (j < dim))
    return this->data[i][j];
  else {
    std::cerr << "Index [" << i << "," << j << "] is out of range" << std::endl;
    throw std::range_error("Index not valid.");
  }
}

template<class T, unsigned int dim>
smallMatrix<T,dim> smallMatrix<T,dim>::add(const smallMatrix<T,dim>& mat) const{
 //Todo: Implementieren
 smallMatrix<T,dim> ret = smallMatrix(*this); //pack this rein
  for (unsigned int i=0;i<dim;i++) {
    for (unsigned int j=0;j<dim;j++) {
      T currval = mat(i,j);
      ret(i,j) += currval;
    }
  }
  return ret;
}

template<class T, unsigned int dim>
smallMatrix<T,dim> smallMatrix<T,dim>::sub(const smallMatrix<T,dim>& mat) const{
  //selber ansatz, nur mit minus
  smallMatrix<T,dim> ret = smallMatrix(*this); //pack this rein
  for (unsigned int i=0;i<dim;i++) {
    for (unsigned int j=0;j<dim;j++) {
      T currval = mat(i,j);
      ret(i,j) -= currval;
    }
  }
  return ret;
}

template<class T, unsigned int dim>
smallMatrix<T,dim> smallMatrix<T,dim>::scalarMult(const T val) const{
 smallMatrix<T,dim> ret = smallMatrix();
 smallMatrix<T,dim> lhs = smallMatrix(*this);
 for (unsigned int i=0;i<dim;i++) {
    for (unsigned int j=0;j<dim;j++) {
      T currval = lhs(i,j);
      ret(i,j) = val*currval;
    }
  }
  return ret;
}

template<class T, unsigned int dim>
smallMatrix<T,dim> smallMatrix<T,dim>::matMult(const smallMatrix<T,dim>& mat) const{
 //Todo: Implementieren
 smallMatrix<T,dim> ret = smallMatrix();
 smallMatrix<T,dim> lhs = smallMatrix(*this);
 for (unsigned int i=0;i<dim;i++) {
  for (unsigned int k=0;k<dim;k++) {
    for (unsigned int j=0;j<dim;j++) {
      T thi_curr =lhs(i,j);
      T mat_curr = mat(j,k);
      ret(i,k) = ret(i,k) + thi_curr*mat_curr;
    }
  }
 }
 return ret;
}
template<class T, unsigned int dim>
bool smallMatrix<T,dim>::isIdentity(double tol) const{
  //Todo: Implementieren
  bool ret = true;
  smallMatrix<T,dim> lhs = smallMatrix(*this);
  for (unsigned int i=0;i<dim;i++) {
    for (unsigned int j=0;j<dim;j++) {
      T currval = lhs(i,j);
      currval = abs(currval);
      if (currval>tol) {
        ret = false;
        break;
      }
    }
  }
  return ret;
}

template<class T, unsigned int dim>
smallMatrix<T,dim> smallMatrix<T,dim>::transpose(void) const{
  smallMatrix<T,dim> ret = smallMatrix();
  smallMatrix<T,dim> lhs = smallMatrix(*this);
  for (unsigned int i=0;i<dim;i++) {
    for (unsigned int j=0;j<dim;j++) {
      T currval = lhs(j,i);
      ret(i,j) = currval;
    }
  }
  return ret;
}


template<class T, unsigned int dim>
smallMatrix<T,dim> smallMatrix<T,dim>::operator+(const smallMatrix<T,dim>& mat) const{
  return (this->add)(mat);
}

template<class T, unsigned int dim>
smallMatrix<T,dim> smallMatrix<T,dim>::operator-(const smallMatrix<T,dim>& mat) const{
  return (this->sub)(mat);
}

template<class T, unsigned int dim>
smallMatrix<T,dim> smallMatrix<T,dim>::operator*(const T val) const{// Note we only have scalar mult. from the right..
  return (this->scalarMult)(val);
}

template<class T, unsigned int dim>
smallMatrix<T,dim> smallMatrix<T,dim>::operator*(const smallMatrix<T,dim>& mat) const{
  return (this->matMult)(mat);
}


  

// general templated det() method throws an exception:
template<class T, unsigned int dim>
T smallMatrix<T,dim>::det(void) const {
  std::cerr << "Determinant method is not implemented for dim = "
            << dim << std::endl;
  throw std::domain_error("Method not implemented.");
}

// fully specified template methods for T=double and dim=1,2,3
// (note that you cannot, without tricks, partially specify a
// templated method; only the full class can be partially specified)
template<>
double smallMatrix<double, 1u>::det(void) const {
  return this->data[0][0];
}

template<>
double smallMatrix<double, 2u>::det(void) const {
  return (this->data[0][0]*this->data[1][1]
          - this->data[0][1]*this->data[1][0]);
}

template<>
double smallMatrix<double, 3u>::det(void) const {
  return (this->data[0][0]*this->data[1][1]*this->data[2][2]
          + this->data[0][1]*this->data[1][2]*this->data[2][0]
          + this->data[0][2]*this->data[1][0]*this->data[2][1]
          - this->data[0][0]*this->data[1][2]*this->data[2][1]
          - this->data[0][2]*this->data[1][1]*this->data[2][0]
          - this->data[0][1]*this->data[1][0]*this->data[2][2]);
}

