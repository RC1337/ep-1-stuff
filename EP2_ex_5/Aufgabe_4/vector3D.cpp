#include <stdexcept>
#include <cmath>
#include <sstream>
#include <algorithm>
#include "vector3D.hpp"


//Constructors 
//Hinweis: Nutzen Sie Initialisierungslisten und Constructor Delegation
vector3D::vector3D(void) {
	std::tuple<double,double,double> nil (0.0,0.0,0.0);
	this->coords = nil;} 
vector3D::vector3D(std::tuple<double, double, double> _coords){
	this->coords = _coords;
}

vector3D::vector3D(const vector3D& other){
	this->coords = other.coords;
}


void vector3D::setCoords(std::tuple<double, double, double> _coords) {
	this->coords = _coords;
}

void vector3D::setCoords(char coord, double val) {
	//Todo: Implementieren
	std::tuple<double,double,double> co = this->coords;
	if (coord == 'x') {
		std::get<0>(co) = val;
	}
	else if (coord == 'y' ) {
		std::get<1>(co) = val;
	}
	else if (coord == 'z') {
		std::get<2>(co) = val;
	}
	else {
		throw std::invalid_argument("unbekannte koordinate gegeben");
	}
	this->coords = co;
}

std::tuple<double, double, double> vector3D::getCoords(void) const {
	return this->coords;
}

double  vector3D::getCoords(char coord) const {
	double ret = 0; //default
	std::tuple<double,double,double> co = this->coords;
	if (coord == 'x') {
		ret = std::get<0>(co);
	}
	else if (coord == 'y') {
		ret = std::get<1>(co);
	}
	else if (coord == 'z') {
		ret = std::get<1>(co);
	}
	else {
		throw std::invalid_argument("unbekannte koordinate gegeben");
	}
	return ret;
}
std::ostream& operator<<(std::ostream& os, const vector3D& vec) {
	std::tuple<double,double,double> co = vec.coords;
	os << "( " << std::get<0>(co) << ", " << std::get<1>(co) << ", " << std::get<2>(co) << ")" << std::endl;
	return os;
}

void vector3D::add(const vector3D& vec) {
	std::tuple<double,double,double> lhs = this->coords;
	std::tuple<double,double,double> rhs = vec.coords;
	std::get<0>(lhs) = std::get<0>(lhs) + std::get<0>(rhs);
	std::get<1>(lhs) = std::get<1>(lhs) + std::get<1>(rhs);
	std::get<2>(lhs) = std::get<2>(lhs) + std::get<2>(rhs);
	this->coords = lhs;
}


void vector3D::sub(const vector3D& vec) {
	std::tuple<double,double,double> lhs = this->coords;
	std::tuple<double,double,double> rhs = vec.coords;
	std::get<0>(lhs) = std::get<0>(lhs) - std::get<0>(rhs);
	std::get<1>(lhs) = std::get<1>(lhs) - std::get<1>(rhs);
	std::get<2>(lhs) = std::get<2>(lhs) - std::get<2>(rhs);
	this->coords = lhs;

}

void vector3D::scalarMult(double val) {
	std::tuple<double,double,double> lhs = this->coords;
	std::get<0>(lhs) = val*std::get<0>(lhs);
	std::get<1>(lhs) = val*std::get<1>(lhs);
	std::get<2>(lhs) = val*std::get<2>(lhs);
	this->coords = lhs;
}

void vector3D::scalarDiv(double val) {
	//schließe nullteiler aus über fehlerausgabe
	if (val == 0.0) {
		throw std::domain_error("Kann nicht durch null teilen");
	}
	else {
		std::tuple<double,double,double> lhs = this->coords;
		std::get<0>(lhs) = std::get<0>(lhs)/val;
		std::get<1>(lhs) = std::get<1>(lhs)/val;
		std::get<2>(lhs) = std::get<2>(lhs)/val;
		this->coords = lhs;
	}
}

vector3D vector3D::crossP(const vector3D& right) {
	std::tuple<double,double,double> lhs = this->coords;
	std::tuple<double,double,double> rhs = right.coords;
	double a = std::get<1>(lhs)*std::get<2>(rhs)-std::get<2>(lhs)*std::get<1>(rhs);
	double b = std::get<2>(lhs)*std::get<0>(rhs)-std::get<0>(lhs)*std::get<2>(rhs);
	double c = std::get<0>(lhs)*std::get<1>(rhs)-std::get<1>(lhs)*std::get<0>(rhs);
	std::tuple<double,double,double> co (a,b,c);
	vector3D res = vector3D(co);
	return res;
}

double vector3D::vectorMult(const vector3D& right) {
	//Todo: Implementieren
	std::tuple<double,double,double> lhs = this->coords;
	std::tuple<double,double,double> rhs = right.coords;
	double res = std::get<0>(lhs)*std::get<0>(rhs)+std::get<1>(lhs)*std::get<1>(rhs)+std::get<2>(lhs)*std::get<2>(rhs);
	return res;
}

double vector3D::norm(void) {
	//da double: euklid-norm einfach wurzel aus Skalarprodukt
	double squared = this->vectorMult(*this);
	double res = sqrt(squared);
	return res;
}//Returns norm of self


void vector3D::normalize(void) {
	//Todo: Implementieren
	//Achtung: Division durch 0 beachten!
	double normval = this->norm();
	double diff = abs(1-normval);
	if (normval < tolerance) {
		std::cout << "Vector is zero-vector, will just pass it through like this" << std::endl;
	}
	else if (diff > tolerance) {
		throw std::domain_error("Normieren wurde mit Abweichung größer Fehlertoleranz gemacht");
	}
	this->scalarDiv(normval);
} 


//Operator Overloads for pretty notation
//Optional: Einkommentieren und Implementieren

void  vector3D::operator+=(const vector3D& vec) {
	//Todo: Implementieren
	this->add(vec);
}

void  vector3D::operator-=(const vector3D& vec) {
	//Todo: Implementieren
	this->sub(vec);
}
void  vector3D::operator*=(double val) {
	//Todo: Implementieren
	this->scalarMult(val);
}
void  vector3D::operator/=(double val) {
	//Todo: Implementieren
	this->scalarDiv(val);
}


vector3D operator+(const vector3D& left, const vector3D& right){
	//Todo: Implementieren
	vector3D ret = vector3D(left.coords);
	ret+=right;
	return ret;
	
}

vector3D operator-(const vector3D& left, const vector3D& right){
	//Todo: Implementieren
	vector3D ret = vector3D(left.coords);
	ret-=right;
	return ret;
}	
vector3D operator*(const vector3D& vec, double val){
	//Todo: Implementieren
	vector3D ret = vector3D(vec.coords);
	ret*=val;
	return ret;
	
}
vector3D operator/(const vector3D& vec, double val){
	//Todo: Implementieren
	vector3D ret = vector3D(vec.coords);
	ret/=val;
	return ret;
	
}



