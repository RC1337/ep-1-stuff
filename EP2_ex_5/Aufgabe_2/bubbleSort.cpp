/*Johannes Bogon 2835480
Michael Lies 2669805*/

#include <iostream>
#include <vector>
#include <ctime> // time
#include <cstdlib> // srand, rand

// Namen eintragen:

template<class T>
std::ostream& operator<< (std::ostream& os, const std::vector<T>& vec)
{
  os << "(";
  for(typename std::vector<T>::const_iterator it = vec.begin(); it != vec.end();)
    {
      os << *it;
      if ( (++it) != vec.end() )
        os << ",";
    }
  os << ")" ;
  return os;
}

template<class T>
void mySort(std::vector<T>& vec)
{
  T temp;
  int swapped = -1;
  int n = vec.size();
  while (swapped != 0) {
    swapped = 0;
    for (int j=0; j<n-1;j++) {
      if (vec[j+1] < vec[j]) {
        temp = vec[j];
        vec[j] = vec[j+1];
        vec[j+1] = temp;
        swapped = -1;
      }
    }
  }
}

#define N 10

int main()
{
  srand(time(NULL));
  
  std::vector<double> vectorDouble(N);
  for(unsigned int i=0; i < N; i++)
    vectorDouble[i] = double(rand()) / RAND_MAX;
  
  std::cout << "Before sorting: vectorDouble = " << vectorDouble << std::endl;
  mySort(vectorDouble);
  std::cout << "After sorting:  vectorDouble = " << vectorDouble << std::endl;
  
  return(0);
}
