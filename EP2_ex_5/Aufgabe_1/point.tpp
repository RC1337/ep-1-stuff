/*Johannes Bogon 2835480
Michael Lies 2669805*/

#include <stdexcept>

template<class T, unsigned int dim>
point<T,dim>::point(void) {
  //point<T,dim> this = new T[dim]; //datenzuweisung: typ T, dim Dimensionen
  for (unsigned int i=0;i<dim;i++) {
    this[i] = 0; //überall null rein
  }
}

template<class T, unsigned int dim>
point<T,dim>::point(const T val) {
  this->fill(val);
  }

template<class T, unsigned int dim>
point<T,dim>::point(const std::initializer_list<T> list)
{
  if (list.size() != this->size()) {
    throw std::logic_error("dimension is wrong");
  }
  else {
    unsigned int i = 0;
    for (auto j = list.begin();j != list.end(); ++j) {
      this->at(i) = *j;
      i += 1;
    }
    
    //frage: datentyp - betrachte ich nicht, muss ich den ändern?
    }
  }

template<class T, unsigned int dim>
T point<T,dim>::scalar(const point<T,dim>& other) const
{
  //Ich nehm an, hier kann mit den dims nichts schiefgehen, deswegen darf man ignorieren?
  T rvalue = T(0);
  for (unsigned int i = 0;i<this->size();i++) {
    T leftval = this->at(i);
    T rightval = other[i];
    T val = leftval*rightval;
    rvalue += val;

  }
  return rvalue;
}

template<class T, unsigned int dim>
point<T,dim>& point<T,dim>::operator=(const point<T,dim> & src)
{
  /*if (this->size() != src.size()) {
    delete[] this;
    this = (src.size());
  } */
  for (unsigned int i=0;i<this->size();i++) {
    this[i] = src[i];
  }
  return *this;
}

// general templated cross() method throws an exception:
template<class T, unsigned int dim>
point<T,dim> point<T,dim>::cross(const point<T,dim>& other) const {
  throw std::domain_error("not implemented for this dimension");
}

// fully specified template methods for T=double and dim=3
// (note that you cannot, without tricks, partially specify a
// templated method; only the full class can be partially specified)
template<>
point<double,3u> point<double,3u>::cross(const point<double,3u>& other) const
{
  //Kreuzprodukt in 3 dims
  point<double,3u> ans(3);

  ans[0] = this->at(1)*other[2] - this->at(2)*other[1];
  ans[1] = this->at(2)*other[0] - this->at(0)*other[2];
  ans[2] = this->at(0)*other[1] - this->at(1)*other[0];
  return ans;
}