/*Johannes Bogon 2835480
Michael Lies 2669805*/
#ifndef POINT_HPP
#define POINT_HPP

#include <iostream>
#include <stdexcept>
#include <array>
#include <functional>
#include <algorithm>

template<class T, unsigned int dim>
class point : public std::array<T,dim> {
public:
  // default constructor (zero matrix)
  point(void);
  point(const T);
  point(const std::initializer_list<T>);
  point<T,dim>& operator=(const point<T,dim>&);
  T scalar(const point<T,dim>&) const;
  point<T,dim> cross(const point<T,dim>&) const;
  friend point<T,dim> operator+(const point<T,dim> first, const point<T,dim>& other)
  {
    point<T,dim> rvalue(first.size());
    for (unsigned int i=0;i<rvalue.size();i++) {
      rvalue[i] = first[i] + other[i];
    }
    return rvalue;
  }
  friend point<T,dim> operator-(const point<T,dim> first, const point<T,dim>& other)
  {
    point<T,dim> rvalue(first.size());
    for (unsigned int i=0;i<rvalue.size();i++) {
      rvalue[i] = first[i] - other[i];
    }
    return rvalue;
  }
  friend std::ostream& operator<<(std::ostream& os, const point<T,dim>& other) {
    os << "(" << other[0];
    if (other.size()>1) {
      for (unsigned int i = 1;i<other.size();i++){
        os << ", " << other[i];
      }
    }
    os << ")" << std::endl;
    return os; 
  
  }
};

#endif // POINT_HPP
