#include <iostream>
#include "point.hpp"

int main(void) {
  point<double, 2u> a1(2.);
  std::cout << "a1 = " << a1 << std::endl;
  point<double, 2u> b1 = 1; b1[1]=-1;
  std::cout << "b1 = " << b1 << std::endl;
  std::cout << "a1 + b1 = " << a1 + b1 << std::endl;
  std::cout << "a1 - b1 = " << a1 - b1 << std::endl;
  std::cout << "Scalar product between a1 and b1 is " << a1.scalar(b1)
              << std::endl;
  //std::cout << "Cross product between a1 and b1 is " << a1.cross(b1)
  //          << std::endl;
  point<double, 3u> a2({3.,1.,4.});
  std::cout << "a2 = " << a2 << std::endl;
  point<double, 3u> b2(1.); 
  std::cout << "b2 = " << b2 << std::endl;
  std::cout << "a2 + b2 = " << a2 + b2 << std::endl;
  std::cout << "a2 - b2 = " << a2 - b2 << std::endl;
  std::cout << "Scalar product between a2 and b2 is " << a2.scalar(b2)
            << std::endl;
  std::cout << "Cross product between a2 and b2 is " << a2.cross(b2)
            << std::endl;

  return 0;
}
