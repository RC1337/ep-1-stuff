#include <stdio.h>
#include <stdlib.h>
//#include <gmp.h>

#include "mini-mpq.h"
#include "mini-gmp.h"

#include "gauss.h"

mpq_t** read_matrix(char* fname, int* n) {
    //um matrix einzulesen
    printf("we are doing something");

    //initialisiere die matrix
    mpq_t **M;
    M = malloc((*n+1) * sizeof ( mpq_t *));
    if (!M) {
		printf("Error! Memory not allocated!");
		exit(0);
	}
    for (int i = 0;i<*n;i++) {
        M[i] = malloc(*n * sizeof(mpq_t *));
        if (! M[i]) {
			printf("Error, memory not allocated!");
			exit(0);
		}
    }
    M[*n] = malloc(sizeof(mpq_t *));
    if (!M[*n]) {
		printf("Error! Memory not allocated!");
		exit(0);
	}

    FILE* file;
    file = fopen(fname,"r");
    char num[1000];
    for (int i = 0; i < *n; i++) {
        for (int j = 0; j<*n; j++) {
            fscanf(file," %s",num);
            mpq_init(M[i][j]);
            mpq_set_str(M[i][j],num,10); //base 10
        }
    }
    mpq_set_str(M[*n][0],num,10); //encoding the dimension as extra line at end
    
    return M;
}

void write_matrix(mpq_t **M, int n) {
    //M hat n Zeilen und n+1 Spalten, und n+1-te Zeile in der n steht, weil das so verlangt war
    //wir geben aber nur die tatsächliche Matrix aus und nicht die Zeile mit n drin
    mpq_t counter;
    for (int i = 0; i<n;i++) { //Iteration über Zeilen
        for (int j = 0;j<n;j++) {
            //erstmal nur die Werte für A, b packen wir am Ende etwas abgesetzt dran
            gmp_printf("%Qd ", M[i][j]);
        }
        printf("   ");
        gmp_printf("%Qd\n",M[i][n]); //das ist b, danach neue Zeile
    }
}

void subtract_row(mpq_t lambda, mpq_t ** M, int n, int source, int target) {
    //abziehen lamda mal Zeile source von Zeile target
    mpq_t subtractor;
    mpq_init(subtractor);
    mpq_t curr_subtr;
    mpq_init(curr_subtr);
    for (int i = 0;i<n+1;i++) {
        mpq_set(curr_subtr,M[target][i]);
        mpq_mul(subtractor,lambda,curr_subtr);
        mpq_sub(M[source][i],M[source][i],M[target][i]);
    }
}

void normalize_row(mpq_t** M, int n, int i) {
    //teilt alles in Zeile i durch M[i,i]
    mpq_t pivot;
    mpq_init(pivot);
    mpq_set(pivot,M[i][i]);
    for (int j=0;j<n+1;j++) {
        mpq_div(M[i][j],M[i][j],pivot);
    }
}

void forward_elim_step(mpq_t** M,int n, int i) {
    //elimiere durch addieren von Vielfachen der Zeile i alles unterhalb von M[i][i]
    //Iteration durch Zeilen
    mpq_t pivot;
    mpq_init(pivot);
    mpq_set(pivot,M[i][i]);
    mpq_t zero;
    mpq_init(zero);
    mpq_set_str(zero,"0",10);
    for (int j = i+1;j<n;j++) {
        //was ist der Wert da?
        mpq_t pwert;
        mpq_init(pwert);
        mpq_set(pwert,M[j][i]);
        //wenn der wert null ist - nichts tun, sonst abziehen
        if (! mpq_cmp(pwert,zero)) {
            //note: wir nehmen an, dass wir keine null-pivots bekommen, sonst können hier fehler kommen
            mpq_t lambda;
            mpq_init(lambda);
            mpq_div(lambda,pwert,pivot); //wollen abziehen tats. Wert durch pivot um j,i loszuwerden
            subtract_row(lambda,M,n,i,j); //source i, target j, rest bewegt sich nicht
        }
    }
}

void backward_elim_step(mpq_t** M,int n, int i) {
    //eliminiere alles oberhalb M[i][i]
    mpq_t pivot;
    mpq_init(pivot);
    mpq_set(pivot,M[i][i]);
    mpq_t zero;
    mpq_init(zero);
    mpq_set_str(zero,"0",10);
    //über Zeilen
    for (int j=i-1;j>=0;j--) {
        //was ist der Wert da?
        mpq_t pwert;
        mpq_init(pwert);
        mpq_set(pwert,M[j][i]);
        //wenn der wert null ist - nichts tun, sonst abziehen
        if (! mpq_cmp(pwert,zero)) {
            //note: wir nehmen an, dass wir keine null-pivots bekommen, sonst können hier fehler kommen
            mpq_t lambda;
            mpq_init(lambda);
            mpq_div(lambda,pwert,pivot); //wollen abziehen tats. Wert durch pivot um j,i loszuwerden
            subtract_row(lambda,M,n,i,j); //source i, target j, rest bewegt sich nicht
        }
    }

}

int find_pivot(mpq_t** M,int n, int j) {
    //for simplicity nehmen wir einfach den ersten wo es nicht null ist, auch wenn das
    //an sich eher nicht optimal ist
    mpq_t zero;
    mpq_init(zero);
    mpq_set_str(zero,"0",10);
    int piv_index = -1;//default case: index that does not exist
    int i = j;
    while (i<n) {
        if (! mpq_cmp(zero,M[i][j])) {
            piv_index = i;
            break;
        }
        i++;
    }
    return piv_index;
    //note: wenn es kein pivot gibt, geben wir einen ungültigen index zurück. an sich produziert das 
    //dann mist; elegant wäre eine Fehlermeldung "gibt kein Pivot" oder so
}

void gauss(mpq_t ** M, int n) {
    /*Vorgehen:
    - iteriere durch die Spalten
    - in jeder Spalte: finde die erste Spalte, wo das pivot nicht null ist
    - tausche die an die richtige Position
    - dann forward-iteration*/
    for (int k =0;k<n;k++) {
        int piv;
        piv = find_pivot(M,n,k);
        //is pivot already the correct one? - next step. else first a row swap
        if (piv != k) {
            for (int mm=0;mm<n;mm++) {
                mpq_swap(M[k][mm],M[piv][mm]);
            }
        }
        //forwardelimination jetzt
        forward_elim_step( M,n,k);
    }
    //Jetzt sollte da Diagonalmatrix D * x = b stehen
    //mit backwards-elimination jetzt alles, was nicht auf der Diagonale ist, loswerden
    //um pivots müssen wir uns jetzt keine Gedanken mehr machen, das ist jetzt schon schön sortiert
    for(int k=n-1;k>=0;k--) {
        backward_elim_step(M,n,k);
    }
    //und dann hat es das Ergebnis, was wir wollen
}

int main() {

    //File und n spezifizieren
    char filename[100];
    printf("Geben Sie filename für die zu betrachtende Matrix ein: \n");
    scanf("%s", filename);
    int *dim;
    printf("Geben Sie die Matrixdimension ein: \n");
    scanf("%d",dim);

    //Matric einlesen
    mpq_t** Mat =  read_matrix(filename, dim);
    //Lösen
    gauss(Mat, *dim);
    //Anzeigen
    write_matrix(Mat, *dim);


}