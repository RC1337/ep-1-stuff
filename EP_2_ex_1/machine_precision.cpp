#include <cmath>  // for std::pow(), the power function with integer exponent
                  // in double and float arithmetic.
#include <iostream>

int machine_precision_float(void) {
  //TODO: Implement function machine_precision_float
  
  float base = 2;
  float exp = 0;
  float curr_num = 0; 
 
  while (true) { 
    /*potentially infinite loop, given application trust it will end */
    curr_num = std::pow(base,exp);
    if (1 + curr_num > 1) {
      //continue
      exp--;
    }
    else {
      //hit the genauigkeit, print it out and stop
      std::cout << "float-precision: " << curr_num << std::endl;
      break;
    }
  }
  return 0;
  

}

int machine_precision_double(void) {
  //TODO: Implement function machine_precision_double

  double base = 2;
  double exp = 0;
  double curr_num = 0;
  while (true) { /*potentially infinite loop, given application trust it will end */
    curr_num = std::pow(base,exp);
    if (1 + curr_num > 1) {
      //continue
      exp--;
    }
    else {
      //hit the genauigkeit, print it out and stop
      std::cout << "double-precision: " << curr_num << std::endl;
      break;
    }
  } 
  return 0;

}

int main(void) {
  /*
    Test your own implementation here.
    Your output should consist of two lines of the following form
float: X.XXXXXXe-XX
double: X.XXXXXXe-XX
    where X.XXXXXXe-XX denotes the machine precision printed in exponential form.
  */

  machine_precision_float();
  machine_precision_double();
}

