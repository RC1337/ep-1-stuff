#include "diffquot.hpp"
#include "diffquot_function.hpp"  // defines the function f(x) and
                                  // its derivative df(x)
#include <cmath>  // for std::fabs()
#include <sstream>
#include <string>
#include <iostream>

double numdiff(func *f, double x, double h) {
  //TODO: Implement function numdiff

  double num_val = 0;
  if (h == 0) { //errorproofing h
    std::cout << "zero step-size, not valid" << std::endl;
  }
  else {
    num_val = (f(x+h)-f(x))/h;
  }
  return num_val;
}

int main(int argc, char *argv[]) {
  
  /*
    Test your own implementation here.
    You can use std::cout to print values. 
    The output in this function should be in lines of the form:
h = 1.000000e-01, num. derivative = 4.973638e-01, error = 4.293855e-02
  */

  int ret = 0;
  //give correct returns:

  //argv hat nur programmname, no arguments (i.e. only max one elt in argv)
  if (argc == 1) { ret = -1;}

  else {
    //argument is negative
    if (argv[1] < 0) {
      ret = -2;
    }
    else {
      //it is fine, do the actual stuff
      ret = 0;

      char* ptr;
      int N = std::strtol(argv[1],&ptr,10);
            
      double h = 0;

      double corr_val = df(1.0);
      std::cout << "correct: " << corr_val << std::endl;
      double num_val = 0;
      double diff = 0;

      //now do the iteration
      for (int i = 0; i<=N; i++) {
        h = std::pow(10.0,-i);
        num_val = numdiff(f,1.0,h);
        diff = std::fabs(corr_val-num_val);
        std::cout << "h = " << std::scientific << h << "  num. derivative = " << std::scientific << num_val 
        << "  error = " << std::scientific << diff << std::endl;

      }
    }
  }
  
  return ret;
 
}

/*
Kommentar zur Zusatzaufgabe:
Bis Schrittweite e-08 sieht man in jedem Schritt ca eine Abnahme um eine Größenordnung
Danach wird das Bild unklar, und der Fehler wird größer. Die Schrittweite entspricht der Genauigkeit
von float, obwohl wir in double sind. Allerdings führen wir ja bei der Funktion auch Operationen aus
(es ist davon auszugehen, dass das im Computer durch Taylor angenähert wird, also steckt da eine 
Multiplikation mit drin), dadurch kommen wir nicht auf eine Arbeitsgenauigkeit von 16, sondern von 8
mit dem Verfahren
*/