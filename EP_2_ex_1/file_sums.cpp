#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>

double get_sum(std::string file) {
  //TODO: Implement function get_sum
  
  std::string path = "../files/";
  path.append(file.data());

  double sum;
  sum = 0;
  
  //open f-stream to work with file
  std::ifstream fs(path);
  std::string line;
  
  if (fs.is_open()) {
    while(std::getline(fs,line)) {
      std::string::size_type st;
      double val = std::stod(line,&st);
      sum = sum + val;
    }


  }
  else {
    sum = -1;
  }

  return sum;

  

}

int main(void) {
  /*
    Test your own implementation here.
    You can use std::cout to print values. 
  */
  std::string test1 = "data1";
  double n1 = get_sum(test1);
  std::cout << "Value for File 1: " << std::setprecision(16) << std::scientific << n1 << std::endl;

  std::string test2 = "data2";
  double n2 = get_sum(test2);
  std::cout << "Value for File 2: " << n2 << std::endl;

  std::string test3 = "data3";
  double n3 = get_sum(test3);
  std::cout << "Value for File 3: " << n3 << std::endl;

  int ret = 1;
  return ret;
}

/*
Kommentar für Zusatz:
Bei den hinteren Nachkommastellen gibt es Probleme mit der Genauigkeit. Was ich nehmen sollte, kommt
druuf an was ich will. Bei der aufsteigenden Sortierung werde ich höher herauskommen als bei der ab-
steigenden (die kleinen Zahlen verschwinden am Ende in den hinteren NKS), sollte ich also machen,
wenn mein Wert möglichst hoch sein soll. Beim Unsortierten ist nicht vorherzusagen, wie sich die 
Fehler auswirken*/