#include "mat_mult.h"
//N is defined as 4


/* Writes the product of a nxm matrix A, given as a flat (1-dimensional) array of length n*m
   as specified on the exercise sheet.
   and a mxo matrix B, given as a flat array of length m*o
   to the array C
*/
void mat_mult(int n, int m, int o, int A[], int B[],int C[]) {
    /*gen form mat-mult: c_ik = sum_j=1^m a_ij*bjk
    Format of C: n,o (n,m*m,o), so array of length n*o
    */

    for (int i = 0; i<n;i++)
    {
        for (int j = 0; j<o;j++)
        {
            int sum = 0;
            for (int k = 0; k < m; k++)
            {
                sum = sum + A[i*m+k]*B[k*o+j];
            }
            C[i*o+j] = sum;
        }
    }

}
