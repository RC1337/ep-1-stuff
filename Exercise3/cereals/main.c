#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "read_cereals.h"

//global variables
char name[74][30];
int calories[74];
double fibre[74];
double rating[74];
int position[74]; //not entirely sure if they want that global, but seems better

//read cereals function
int read_cereals(){
    //point to input
    FILE *fileptr;
    //open input
    fileptr = fopen("cereals.in","r");
    if (fileptr == NULL) {
        fprintf(stderr, "Kann cereals nicht aufmachen");
    }
    
    char * c;
    int i = 1; //use this for position getting  
    
    char buf[1000];
    char current[1000];

   c = fgets(buf, 1000, fileptr);
   
   while (c != NULL)
    { 
        //we now managed to read the stuff line by line
        strcpy(current,c);
        //set position
        position[i] = i; // works, returns numbers 1-74
       
       //now work with the lines and set the lists:
       char* token = strtok(current, " ");
       char tok[30];
       strcpy(tok,token);
       int j = 1;
       strcpy(name[i],tok);
       
       while (token != NULL) {
        token = strtok(NULL," ");
        if (j == 1) { //calory list
            char tok[30];
            strcpy(tok,token);
            int nummer = atoi(tok);
            calories[i] = nummer;
            j = j+1;

        }
        else {
            if (j == 2) { //fibre list
                char tok[30];
                strcpy(tok,token);
                double nummer = atof(tok);
                fibre[i] = nummer;
                j = j+1;

            }
            else {
                if (j == 3) { //rating list
                    char tok[30];
                    strcpy(tok,token);
                    double nummer = atof(tok);
                    rating[i] = nummer; 
                    break;   
                }
            }
        }
       }

        i = i+1;
        c = fgets(buf, 1000, fileptr);
    }
    fclose(fileptr);
    return 0;
}

int write_cereals () {
    //write the cereals in a new data-set

    //output aufmachen, mit schreibzugriff
    FILE *ofptr;
    //Version f. ersten Teil, kommentiert um datei zu behalten
    //ofptr = fopen("cereals.out", "w");
    ofptr = fopen("cereals_by_calories.out","w");
    //info falls fehler dabei
    if (ofptr == NULL){
        fprintf(stderr, "Kann output-file nicht aufmachen");
    }

    //zeug wieder eingeben
    char curr_name[30];
    int curr_cal;
    char c_cal[1000];
    double curr_fibre;
    char c_fibre[1000];
    double curr_rating;
    char c_rating[1000];
    char space[] = " ";
    char empty[] = "";
    char curr_line[1000];

    int i = 1;
    while (i<=74)
    {
        strcpy(curr_name,name[i]);
        curr_cal = calories[i];
        sprintf(c_cal, "%d", curr_cal);
        curr_fibre = fibre[i];
        sprintf(c_fibre, "%f", curr_fibre);
        curr_rating = rating[i];
        sprintf(c_rating, "%f", curr_rating);


        //schreib es in die datei
        //first, empty curr_line
        strcpy(curr_line,empty);
        strcat(curr_line, curr_name);
        strcat(curr_line, space);
        strcat(curr_line, c_cal);
        strcat(curr_line,space);
        strcat(curr_line, c_fibre);
        strcat(curr_line,space);
        strcat(curr_line,c_rating);
        fputs(curr_line,ofptr);
        fputc('\n',ofptr);
        i = i+1;


    }

    fclose(ofptr);
    return 0;
}

int compare_calories (int i, int j) {

    if (calories[i] < calories[j]) {
        return -1;
    }
    else {
        if (calories[i] > calories[j]) {
            return 1;
        }
        else {
            return 0;
        }
    }
}

int compare_fibre (int i, int j) {
   
    if (fibre[i] > fibre[j]) {
        return -1;
    }
    else {
        if (fibre[i] < fibre[j]) {
            return 1;
        }
        else {
            return 0;
        } 
    }
}

int compare_rating (int i, int j) {

    if (rating[i] > rating[j]) {
        return -1;
    }
    else {
        if (rating[i] < rating[j]) {
            return 1;
        }
        else {
            return 0;
        } 
    }
}

void sort_cereal( int (* compare)(int, int)){
    //bubblesort on the list, use that we know it is 74 for long
    for (int k=0;k<73;k++) {
        for (int j = k+1;j<74;j++) {
            int val;
            val = compare(k,j);
            //k is the one that is more left
            //if k is better than j, we need to move, else can stay
            if (val == -1) {
                //swap names, cals, fibres etc
                char temp_name[30];
                strcpy(temp_name,name[k]);
                strcpy(name[k],name[j]);
                strcpy(name[j],temp_name);
                int temp_cal;
                temp_cal = calories[k];
                calories[k] = calories[j];
                calories[j] = temp_cal;
                double temp_fibre;
                temp_fibre = fibre[k];
                fibre[k] = fibre[j];
                fibre[j] = temp_fibre;
                double temp_rate;
                temp_rate = rating[k];
                rating[k] = rating[j];
                rating[j] = temp_rate;
            }
        }
    }

    
}

void main () {
   int reader = read_cereals();
   sort_cereal(*compare_calories);
   int wrtier = write_cereals();
}