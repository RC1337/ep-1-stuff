#include <stdio.h>
#include <stdlib.h>
#include "bubblesort.h"

void print_arr(int n, int* arr) {
  for (int i = 0; i < n; i++) {
    printf("%i\n",arr[i] );
  }
}

int main(void) {
  int n = 9;
  int arr[]={200, 34,-51,16,12,123,54,-34,0};
  bubblesort(n,arr);
  print_arr(n,arr);
}
